package net.bgb.monitoring.dao;

import net.bgb.monitoring.connection.Database;
import net.bgb.monitoring.service.RoleService;
import net.bgb.monitoring.web.util.Constants;
import net.bgb.monitoring.web.util.EncryptionUtil;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpSession;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Al-Amin on 3/9/2017.
 */
@Repository
public class AdminDao {

    public boolean login(String username, String password, HttpSession session) throws SQLException {
        Database db = new Database();
        db.connect();
        try {
            Statement stmt = db.connection.createStatement();

            if (username != null && password != null) {

                int level = RoleService.getLevel(username.split("_")[0]);
                if(level == 0) {
                    return false;
                }

                String tableName = "admin";
                if(level==2) tableName+="_l2";
                if(level==3) tableName+="_l3";
                if(level==4) tableName+="_l4";
                if(level==5) tableName+="_l5";

                String query = "select * from "+tableName+" where username like '" + username + "'";
                ResultSet rs = stmt.executeQuery(query);
                while (rs.next() == true && EncryptionUtil.matchWithSecuredHash(password, rs.getString("password"))) {
                    session.setAttribute("userId", rs.getString("id"));
                    session.setAttribute("username", username);
                    session.setAttribute("adminLevel", level);
                    session.setAttribute("centerPointX",rs.getString("center_point_x"));
                    session.setAttribute("centerPointY",rs.getString("center_point_y"));
                    session.setAttribute("autoZoom", Constants.getAutoZoom(level));
                    session.setAttribute("viewUsername", username);
                    session.setAttribute("viewSoldier",null);
                    db.close();
                    return true;
                }
            }
        }catch(Exception e){
        }finally {
            db.close();
        }

        return false;
    }

    public void setPass(String username, String hashedPass) throws SQLException {
        Database db = new Database();
        db.connect();
        try {
            Statement stmt = db.connection.createStatement();
            String query = "update admin set password='" + hashedPass + "' where username like '" + username + "'";
            stmt.executeUpdate(query);
        }catch (Exception e) {

        }finally {
            db.close();
        }

        return;
    }
}
