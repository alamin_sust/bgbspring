package net.bgb.monitoring.service;

import net.bgb.monitoring.connection.Database;
import net.bgb.monitoring.web.util.Constants;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created by Al-Amin on 4/1/2017.
 */
@Service
public class RoleService {



    public static boolean isViewAuthorized(String username, String battalion) throws SQLException {

        Database db = new Database();
        db.connect();
        try {
            String l0_qry=new String();
            Statement l0_st=db.connection.createStatement();
            ResultSet l0_rs;
            String l1_qry=new String();
            Statement l1_st=db.connection.createStatement();
            ResultSet l1_rs;
            String l2_qry=new String();
            Statement l2_st=db.connection.createStatement();
            ResultSet l2_rs;
            String l3_qry=new String();
            Statement l3_st=db.connection.createStatement();
            ResultSet l3_rs;
            String l4_qry=new String();
            Statement l4_st=db.connection.createStatement();
            ResultSet l4_rs;
            String l5_qry=new String();
            Statement l5_st=db.connection.createStatement();
            ResultSet l5_rs;

            int level = getLevel(username.split("_")[0]);

            if(level==0){
                return false;
            }

            int currentLevel = 5, nowParent = 1;
            String currentUsername = new String();

            /*l0_qry="select * from registered_mobiles where battalion like '"+battalion+"'";
            l0_rs = l0_st.executeQuery(l0_qry);
            l0_rs.next();
            nowParent = l0_rs.getInt("under_l5");*/

            if(currentLevel>=level) {
                //l5_qry="select * from admin_l5 where id="+nowParent;
                l5_qry="select * from admin_l5 where username LIKE '"+Constants.LEVEL_5_ADMIN_PREFIX+"_"+battalion+"%'";
                l5_rs = l5_st.executeQuery(l5_qry);
                l5_rs.next();
                nowParent = l5_rs.getInt("boss");
                currentUsername = l5_rs.getString("username");
                currentLevel--;
            }
            if(currentLevel>=level) {
                l4_qry="select * from admin_l4 where id="+nowParent;
                l4_rs = l4_st.executeQuery(l4_qry);
                l4_rs.next();
                nowParent = l4_rs.getInt("boss");
                currentUsername = l4_rs.getString("username");
                currentLevel--;
            }
            if(currentLevel>=level) {
                l3_qry="select * from admin_l3 where id="+nowParent;
                l3_rs = l3_st.executeQuery(l3_qry);
                l3_rs.next();
                nowParent = l3_rs.getInt("boss");
                currentUsername = l3_rs.getString("username");
                currentLevel--;
            }

            if(currentLevel>=level) {
                l2_qry="select * from admin_l2 where id="+nowParent;
                l2_rs = l2_st.executeQuery(l2_qry);
                l2_rs.next();
                nowParent = l2_rs.getInt("boss");
                currentUsername = l2_rs.getString("username");
                currentLevel--;
            }

            if(currentLevel>=level) {
                l1_qry="select * from admin where id="+nowParent;
                l1_rs = l1_st.executeQuery(l1_qry);
                l1_rs.next();
                nowParent = l1_rs.getInt("boss");
                currentUsername = l1_rs.getString("username");
                currentLevel--;
            }
            db.close();
            if(currentUsername.equals(username.split("_")[0]+"_"+username.split("_")[1])) {

                return true;
            }
            else return false;

        }catch (Exception e) {
            db.close();
            return false;
        }
    }

    public static int getLevel(String prefix) {

        if(Constants.LEVEL_5_ADMIN_PREFIX.equals(prefix)) {
            return 5;
        }
        if(Constants.LEVEL_4_ADMIN_PREFIX.equals(prefix)) {
            return 4;
        }
        if(Constants.LEVEL_3_ADMIN_PREFIX.equals(prefix)) {
            return 3;
        }
        if(Constants.LEVEL_2_ADMIN_PREFIX.equals(prefix)) {
            return 2;
        }
        if(Constants.LEVEL_1_ADMIN_PREFIX.equals(prefix)) {
            return 1;
        }

        return 0;
    }

    public static ArrayList<String> getIdLevelUsernamesOfAdmin(int level, String username, int type) throws SQLException {
        Database db = new Database();
        db.connect();
        try {
            /*level =4;
            username = "sector_sylhet";
            */String qry=new String();
            Statement st=db.connection.createStatement();
            ResultSet rs;

            String table = "admin";

            if(level>1) {
                table+="_l"+Integer.toString(level);
            }

            qry = "select * from "+table+" where username LIKE '"+username.split("_")[0]+"_"+username.split("_")[1]+"'";
            rs=st.executeQuery(qry);
            rs.next();

            String qry2=new String();
            Statement st2=db.connection.createStatement();
            ResultSet rs2;
            if(level==5 && type ==1) {
                qry2 = "select * from soldier where battalion like '"+username.split("_")[1]+"'";
            }else if(level == 5 && type==2) {   //specially for pillar
                qry2 = "select * from pillar where battalion like '"+username.split("_")[1]+"'";
            }
                else {
                qry2= "select * from admin_l"+Integer.toString(level+1)+" where boss="+rs.getString("id");
            }
            rs2=st2.executeQuery(qry2);

            ArrayList<String> idLevelUsername = new ArrayList<>();

            while (rs2.next()) {
                if(level!=5||type!=2) {
                    idLevelUsername.add(rs2.getString("id") + "_" + Integer.toString(level + 1) + "_" + rs2.getString("username"));
                } else {
                    idLevelUsername.add(rs2.getString("id") + "_" + Integer.toString(level + 1) + "_" + rs2.getString("name"));
                }
            }
            db.close();
            return idLevelUsername;

        }catch (Exception e) {
            db.close();
            return null;
        }
    }
}
