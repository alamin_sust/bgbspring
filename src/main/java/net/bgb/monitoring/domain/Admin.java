package net.bgb.monitoring.domain;

import javax.persistence.Entity;
import java.io.Serializable;

/**
 * Created by Al-Amin on 3/9/2017.
 */
@Entity
public class Admin implements Serializable {
    private static final long serialVersionUID = 1L;
    private int id;
    private String username;
    private String password;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
