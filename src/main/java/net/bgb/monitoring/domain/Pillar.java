package net.bgb.monitoring.domain;

import javax.persistence.Entity;
import java.io.Serializable;

/**
 * Created by Al-Amin on 3/10/2017.
 */
@Entity
public class Pillar implements Serializable{
    private static final long serialVersionUID = 1L;
    private int id;
    private String name;
    private int number;
    private String longitude;
    private String latitude;
    private String situation;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getSituation() {
        return situation;
    }

    public void setSituation(String situation) {
        this.situation = situation;
    }
}
