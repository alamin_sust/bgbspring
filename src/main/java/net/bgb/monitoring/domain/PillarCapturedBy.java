package net.bgb.monitoring.domain;

import java.io.Serializable;

/**
 * Created by Al-Amin on 3/10/2017.
 */
public class PillarCapturedBy implements Serializable{
    private static final long serialVersionUID = 1L;
    private int soldierId;
    private String imgUrl;

    public int getSoldierId() {
        return soldierId;
    }

    public void setSoldierId(int soldierId) {
        this.soldierId = soldierId;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
