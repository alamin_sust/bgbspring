package net.bgb.monitoring.domain;

import javax.persistence.Entity;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by Al-Amin on 3/10/2017.
 */
@Entity
public class Capture implements Serializable{
    private static final long serialVersionUID = 1L;
    private int id;
    private int soldierId;
    private int pillarId;
    private String timeCaptured;
    private String situation;
    private String imageUrl;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSoldierId() {
        return soldierId;
    }

    public void setSoldierId(int soldierId) {
        this.soldierId = soldierId;
    }

    public int getPillarId() {
        return pillarId;
    }

    public void setPillarId(int pillarId) {
        this.pillarId = pillarId;
    }

    public String getTimeCaptured() {
        return timeCaptured;
    }

    public void setTimeCaptured(String timeCaptured) {
        this.timeCaptured = timeCaptured;
    }

    public String getSituation() {
        return situation;
    }

    public void setSituation(String situation) {
        this.situation = situation;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
