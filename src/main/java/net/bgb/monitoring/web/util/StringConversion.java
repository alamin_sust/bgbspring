package net.bgb.monitoring.web.util;

/**
 * Created by Al-Amin on 3/19/2017.
 */
public class StringConversion {
    public static String nullReplace(String mainString, String to) {
        if(mainString == null || mainString.trim().equals("null") || mainString.trim().length() == 0) {
            return to;
        }
        return mainString;
    }
}
