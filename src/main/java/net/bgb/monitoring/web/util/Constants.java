package net.bgb.monitoring.web.util;

/**
 * Created by Al-Amin on 3/19/2017.
 */
public class Constants {
    public static final int SOLDIER_STARTING_INDEX = 32;
    public static final int PILLAR_STARTING_INDEX = 37;
    public static final int GPS_THRESHOLD_SEC = 60;

    public static final int REFRESH_MAP_SEC = 60;
    public static final boolean REFRESH_MAP = false;

    public static final int REPORTS_TO_SHOW = 10;
    public static final String NOT_AVAILABLE = "n/a";
    public static final String NOT_PROVIDED = "Not Provided";

    public static final String LEVEL_1_ADMIN_PREFIX = "dg";
    public static final String LEVEL_2_ADMIN_PREFIX = "head";
    public static final String LEVEL_3_ADMIN_PREFIX = "region";
    public static final String LEVEL_4_ADMIN_PREFIX = "sector";
    public static final String LEVEL_5_ADMIN_PREFIX = "battalion";

    public static final int SPECIAL_OPERATIONS_STARTING_INDEX = 37;
    public static final int SPECIAL_OPERATIONS_ENDING_INDEX = 37;

    public static final String MAIN_PILLAR_SHORT_NAME = "MP";
    public static final String MAIN_PILLAR_SHORT_NAME2 = "M";

    public static final int LEVEL_1_ADMIN_AUTOZOOM = 7;
    public static final int LEVEL_2_ADMIN_AUTOZOOM = 10;
    public static final int LEVEL_3_ADMIN_AUTOZOOM = 9;
    public static final int LEVEL_4_ADMIN_AUTOZOOM = 10;
    public static final int LEVEL_5_ADMIN_AUTOZOOM = 12;

    public static int getAutoZoom(int level) {
        if(level==1) return LEVEL_1_ADMIN_AUTOZOOM;
        if(level==2) return LEVEL_2_ADMIN_AUTOZOOM;
        if(level==3) return LEVEL_3_ADMIN_AUTOZOOM;
        if(level==4) return LEVEL_4_ADMIN_AUTOZOOM;
        if(level==5) return LEVEL_5_ADMIN_AUTOZOOM;
        return LEVEL_1_ADMIN_AUTOZOOM;
    }

}
