package net.bgb.monitoring.web.util;

import net.bgb.monitoring.connection.Database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Al-Amin on 4/2/2017.
 */
public class ConverterUtil {
    public static String getBattalionFromImie(String authImie) throws SQLException {
        Database db = new Database();
        db.connect();

        try {
            String qry = new String();
            Statement st = db.connection.createStatement();
            ResultSet rs;

            qry = "select * from registered_mobiles where substr(imie_number,1,15) like '"+authImie.replace("/","").substring(0,15)+"'";
            rs= st.executeQuery(qry);
            if(rs.next()) {
                String ret = rs.getString("battalion");
                db.close();
                return ret;
            }
            else {
                db.close();
                return "0";
            }

        }catch (Exception e) {
            db.close();
            return "0";
        }
    }

    public static String formatPillarName(String name) {

        if(name==null || name.isEmpty()) {
            return name;
        }

        String parts[] = name.split("/");

        if(parts.length==1){
            return parts[0];
        }
        else{
            if(parts[1].toUpperCase().equals(Constants.MAIN_PILLAR_SHORT_NAME)||parts[1].toUpperCase().equals(Constants.MAIN_PILLAR_SHORT_NAME2)) {
                return parts[0];
            }
            else {
                return name;
            }
        }
    }
}
