package net.bgb.monitoring.web.controller;

import net.bgb.monitoring.connection.Database;
import net.bgb.monitoring.service.LoginService;
import net.bgb.monitoring.service.RoleService;
import net.bgb.monitoring.web.util.Constants;
import net.bgb.monitoring.web.util.EncryptionUtil;
import net.bgb.monitoring.web.util.MacBindingUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Al-Amin on 3/9/2017.
 */
@Controller
public class LoginController {

    @Autowired
    LoginService loginService;

    private static final String LOGIN = "login";
    private static final String REDIRECT_HOME = "redirect:/currentLocation?soldier=all&currentLoc=true";

    @GetMapping("/login")
    public String loadLoginPage(HttpSession session) {
        if(session.getAttribute("userId")!=null)
        {
            return REDIRECT_HOME;
        }
        session.setAttribute("loginMsg", null);
        return LOGIN;
    }

    @PostMapping("/login")
    public String login(@RequestParam(value = "username", required = false) String username,
                        @RequestParam(value = "password", required = false) String password,
                        @RequestParam(value = "logout", required = false) String logout,
                        HttpSession session) throws SQLException {

        /*pass reset from login page*/
        /*String hashedPass = EncryptionUtil.generateSecuredHash(password);
        if(username!=null && hashedPass !=null) {
            loginService.setPass(username, hashedPass);
        }*/

        session.setAttribute("loginMsg", null);

        if(logout!=null) {
            session.setAttribute("userId",null);
            session.setAttribute("username",null);
            session.setAttribute("adminLevel",null);
            session.setAttribute("centerPointX",null);
            session.setAttribute("centerPointY",null);
            session.setAttribute("autoZoom",null);
            session.setAttribute("viewUsername",null);
            session.setAttribute("viewSoldier",null);
            return LOGIN;
        }


        if(loginService.login(username,password,session) /*&& MacBindingUtil.isAuthorizedMac()*/) {
            return REDIRECT_HOME;
        }
        else {
            session.setAttribute("loginMsg","Username and Password doesn't match. Try again.");
        }

        return LOGIN;
    }

    @GetMapping("/leveledSignup")
    public String loadLeveledSignupPage() {
        return "leveledSignup";
    }

    @PostMapping("/leveledSignup")
    public String postLeveledSignupPage(@RequestParam(value = "username", required = false) String username,
                                        @RequestParam(value = "password", required = false) String password,
                                        @RequestParam(value = "prefix", required = false) String prefix,
                                        HttpSession session) throws SQLException {
        Database db = new Database();
        db.connect();
        try {
            String qry=new String();
            Statement st=db.connection.createStatement();
            ResultSet rs;




            if (username != null && password != null) {

                int level = RoleService.getLevel(prefix);
                if (level == 0) {
                    session.setAttribute("signupMsg", "Something Went Wrong! Try again1.");
                }

                String tableName = "admin";
                if (level == 2) tableName += "_l2";
                if (level == 3) tableName += "_l3";
                if (level == 4) tableName += "_l4";
                if (level == 5) tableName += "_l5";


                String qry2="select max(id)+1 as mxid from "+tableName;
                Statement st2=db.connection.createStatement();
                ResultSet rs2=st2.executeQuery(qry2);
                rs2.next();

                qry = "insert into "+tableName+" (id,username,password,boss) values ("+rs2.getString("mxid")+",'"+prefix+"_"+username+"','"+EncryptionUtil.generateSecuredHash(password)+"',1)";
                st.executeUpdate(qry);
                session.setAttribute("signupMsgSuccess","Successfully Signed Up");


            } else {
                session.setAttribute("signupMsg","Something Went Wrong! Try again.2");
            }

        }catch (Exception e) {
            db.close();
            session.setAttribute("signupMsg","Something Went Wrong! Try again.");
        }

        return "leveledSignup";
    }


    @GetMapping("/insertMobile")
    public String loadInsertMobilePage() {
        return "insertMobile";
    }

    @PostMapping("/insertMobile")
    public String postInsertMobilePage(@RequestParam(value = "imieNumber", required = false) String imieNumber,
                                        @RequestParam(value = "imieNumber2", required = false) String imieNumber2,
                                        @RequestParam(value = "bop", required = false) String bop,
                                        @RequestParam(value = "battalion", required = false) String battalion,
                                        HttpSession session) throws SQLException {
        Database db = new Database();
        db.connect();
        try {
            String qry=new String();
            Statement st=db.connection.createStatement();
            ResultSet rs;

                qry = "insert into registered_mobiles (imie_number,imie_number_2,bop,battalion,status,under_l5) values ('"+imieNumber+"','"+imieNumber2+"','"+bop+"','"+battalion+"',1,1)";
                st.executeUpdate(qry);
            session.setAttribute("insertMobileMsgSuccess","Successfully Inserted");

        }catch (Exception e) {
            db.close();
            session.setAttribute("insertMobileMsg","Something Went Wrong! Try again.");
        }

        return "insertMobile";
    }


}
