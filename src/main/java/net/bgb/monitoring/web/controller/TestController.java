package net.bgb.monitoring.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by Al-Amin on 3/7/2017.
 */
@Controller
public class TestController {
    private static final String TEST = "test";

    @GetMapping("/test")
    public String goTest(Model model) {
        return TEST;
    }
}
