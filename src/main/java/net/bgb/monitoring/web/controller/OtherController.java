package net.bgb.monitoring.web.controller;

import net.bgb.monitoring.service.AppAuthService;
import org.apache.commons.fileupload.FileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by Al-Amin on 3/11/2017.
 */
@Controller
public class OtherController {

    @Autowired
    AppAuthService appAuthService;

    @GetMapping("/addPillar")
    public String getViewAddPillar() {
        return "addPillar";
    }

    @GetMapping("/addSoldier")
    public String getViewAddSoldier() {
        return "addSoldier";
    }

    @PostMapping("/addPillar")
    public String postViewAddPillar() {
        return "addPillar";
    }

    @PostMapping("/addSoldier")
    public String postViewAddSoldier() {
        return "addSoldier";
    }

    @GetMapping("/currentLocation")
    public String getViewCurrentLocation() {
        return "currentLocation";
    }

    @GetMapping("/customSearch")
    public String getViewCustomSearch() {
        return "customSearch";
    }

    @GetMapping("/customSearchResult")
    public String getViewCustomSearchResult() {
        return "customSearchResult";
    }

    @GetMapping("/pillar")
    public String getViewPillar() {
        return "pillar";
    }

    @GetMapping("/pillarHistory")
    public String getViewPillarHistory() {
        return "pillarHistory";
    }

    @GetMapping("/soldier")
    public String getViewSoldier() {
        return "soldier";
    }

    @GetMapping("/reports")
    public String getViewReports() {
        return "reports";
    }

    @GetMapping("/specialOperations")
    public String getViewSpecialOperations() {
        return "specialOperations";
    }

    @GetMapping("/specialOperationsHistory")
    public String getViewSpecialOperationsHistory() {
        return "specialOperationsHistory";
    }

    @GetMapping("/somethingWentWrong")
    public String getViewSomethingWentWrong() {
        return "somethingWentWrong";
    }

    /*@GetMapping("/queryExecutor")
    public String getQueryExecutor() {
        return "queryExecutor";
    }*/

    /*@GetMapping("/imgTest")
    public String getViewImgTest() {
        return "imgTest";
    }*/

    /** App End Requests and Responses*/

    @PostMapping("/getPillarNames")
    public String postViewGetPillarNames(HttpServletRequest request) throws SQLException {
        if(!appAuthService.isAuthorized(request.getParameter("authImie"))) {
            return "appAuthFailPage";
        }
        return "getPillarNames";
    }

    //
    @PostMapping("/insertPillar")
    public String postViewInsertPillar(HttpServletRequest request) throws SQLException {

        if(!appAuthService.isAuthorized(request.getParameter("authImie"))) {
            return "appAuthFailPage";
        }
        return "insertPillar";
    }

    //
    @PostMapping("/updatePillar")
    public String postViewUpdatePillar(HttpServletRequest request, Model model) throws SQLException, IOException, FileUploadException {
        model.addAttribute("result",appAuthService.processMultipartRequest(request));

        return "updatePillar";
    }

    //
    @PostMapping("/updateSoldierLocation")
    public String postViewUpdateSoldierLocation(HttpServletRequest request) throws SQLException {
        if(!appAuthService.isAuthorized(request.getParameter("authImie"))) {
            return "appAuthFailPage";
        }

        return "updateSoldierLocation";
    }

    //
    @PostMapping("/userLogin")
    public String postViewUserLogin(HttpServletRequest request) throws SQLException {
        if(!appAuthService.isAuthorized(request.getParameter("authImie"))) {
            return "appAuthFailPage";
        }
        return "userLogin";
    }

    //
    @PostMapping("/reportToBGB")
    public String postViewReportToBGB(HttpServletRequest request) throws SQLException {
        /*if(!appAuthService.isAuthorized(request.getParameter("authUsername"))) {
            return "appAuthFailPage";
        }*/
        return "reportToBGB";
    }

    //
    @GetMapping("/appAuthFailPage")
    public String postViewAppAuthFailPage() {
        return "appAuthFailPage";
    }

    //
    @GetMapping("/updateApp")
    public String postViewUpdateAppPage() {
        return "updateApp";
    }


}
