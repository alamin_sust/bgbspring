package net.bgb.monitoring.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 * Created by Al-Amin on 3/8/2017.
 */
@Controller
public class HomeController {

    private static final String REDIRECT_HOME = "redirect:/home";
    private static final String HOME = "home";

    @GetMapping("/")
    public String goHome() {
        return REDIRECT_HOME;
    }

    @GetMapping("/home")
    public String home(Model model) {
        return HOME;
    }
}
