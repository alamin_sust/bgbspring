<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="net.bgb.monitoring.connection.Database"%>


<%@ include file="header.jsp" %>

<%
    Database db = new Database();
    db.connect();
    try{
    if(request.getParameter("name")!=null && request.getParameter("battalion")!=null && !request.getParameter("name").isEmpty() && !request.getParameter("battalion").isEmpty()) {


        Statement st = db.connection.createStatement();
        String mxIdQry = "select max(id)+1 as mxid from pillar";
        ResultSet rs = st.executeQuery(mxIdQry);
        String number  = "1";
        String longitude  = "null";
        String latitude  = "null";
        rs.next();
        String id = rs.getString("mxid");
        String name=request.getParameter("name");
        String battalion = request.getParameter("battalion");

        if(request.getParameter("number")!=null&&request.getParameter("number").length()>0){
            number  = request.getParameter("number");
        }
        if(request.getParameter("longitude")!=null&& request.getParameter("longitude").length()>0 ){
            longitude  = request.getParameter("longitude");
        }
        if(request.getParameter("latitude")!=null&&request.getParameter("latitude").length()>0){
            latitude  = request.getParameter("latitude");
        }
        String query = "insert into pillar(id,name,number,longitude,latitude,battalion) values("+id+",'"+name+"',"+number+", '"+longitude+"', '"+latitude+"','"+battalion+"')";
        Statement stIns = db.connection.createStatement();
        stIns.executeUpdate(query);
    }


%>


<%
      if(request.getParameter("name")!=null && request.getParameter("battalion")!=null && !request.getParameter("name").isEmpty() && !request.getParameter("battalion").isEmpty()){
          %>
<div class="container-fluid text-center">  
     <div class="alert alert-success fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">X</a>
        <p><strong>Success!</strong></p>
        Pillar Inserted Into Central Database Successfully.
      </div>
    <%
        request.setAttribute("name", null);
        
    }%>
  <div class="row content">
    <div class="col-sm-2 sidenav">
    </div>
    <div class="col-sm-8 text-left">
        <legend style="font-size: 35px">Add a New Pillar</legend>
      <br>
      <form class="form-inline" method="post" action="addPillar">
      Name:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input type="text" name="name" class="form-control" size="50" placeholder="Name" required>
      <%--<br><br>
      Number:&nbsp;&nbsp;&nbsp;&nbsp; <input type="number" name="number" class="form-control" size="50" placeholder="Number" >
      <br><br>
      Longitude: <input type="text" name="longitude" class="form-control" size="50" placeholder="Longitude" >
      <br><br>
      Latitude: &nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="latitude" class="form-control" size="50" placeholder="Latitude">--%>
          <br><br>
          Battalion: <input type="text" name="battalion" class="form-control" size="50" placeholder="Battalion" required>
      <br><br>
      <button type="submit" class="btn btn-danger">Insert</button>
      </form>
    </div>
    <div class="col-sm-2 sidenav">
      
    </div>
  </div>
</div>
<br><br><br><br><br><br><br>
<%
    }catch (Exception e){
        response.sendRedirect("somethingWentWrong");
    }finally {
    db.close();
    }


%>
<%@ include file="footer.jsp" %>
