<%@ page import="net.bgb.monitoring.web.util.Constants" %>
<%--
  Created by IntelliJ IDEA.
  User: Al-Amin
  Date: 3/8/2017
  Time: 11:05 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html lang="en">
<%try{
/*Database db = new Database();
        db.connect();*/

%>
<head>
    <title>সীমান্ত রক্ষা</title>
    <meta charset="utf-8">
    <%--<meta http-equiv="refresh" content="5" />--%>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />

    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
    <script>
        $(document).ready(function() {
                $('#datePicker')
                    .datepicker({
                        todayHighlight: true,
                        autoclose: false,
                        format: 'yyyy-mm-dd'
                    })
                    .on('changeDate', function (ev) {
                        // Revalidate the date field
                        $('#datePicker').datepicker('hide');
                    });
            $('.dropdown-submenu a.test').on("click", function(e){
                $(this).next('ul').toggle();
                e.stopPropagation();
                e.preventDefault();
            });
            });
    </script>
    <style>


        /*dropdown*/
        .dropdown-submenu {
            position: relative;
        }

        .dropdown-submenu .dropdown-menu {
            top: 0;
            left: 100%;
            margin-top: -1px;
        }




        /* Set black background color, white text and some padding */
        footer {
            background-color: #555;
            color: white;
            padding: 15px;
        }

        .navbar {
            margin-bottom: 0px;
            border-radius: 0;
            color: white;
        }

        /* Remove the jumbotron's default bottom margin */
        .jumbotron {
            margin-bottom: 0;
            height: 250px;
        }

        .dropdown-menu > li > a:hover {
            background-color: #561a00;
            background-image: none;
        }

        /* Add a gray background color and some padding to the footer */
        footer {
            background-color: #3e3d3f;
            padding: 25px;
        }

        h2 {
            text-shadow: 2px 1px #4d4d4d;
        }



        /* On small screens, set height to 'auto' for sidenav and grid */
        @media screen and (max-width: 767px) {
            .sidenav {
                height: auto;
                padding: 15px;
            }
            .row.content {height:auto;}
        }
</style>

</head>
<body>

    <%
    if(session.getAttribute("userId")==null){
                    response.sendRedirect("login");
                }
%>


    <script>
        var refreshMap = <%=Constants.REFRESH_MAP%>;
        var seconds = <%=Constants.REFRESH_MAP_SEC%>;

        if(refreshMap) {
            function loadDiv() {
                document.getElementById("fulpage").style.display = 'none';
            }

            function autoRefresh_div() {
                //loadDiv();
                $("#fully").load("#fully");// a function which will load data from other file after x seconds
            }

            setInterval('autoRefresh_div()', seconds * 1000); // refresh div after seconds secs
        }
    </script>

<div id="fully">
    <div>
<div class="jumbotron"
     style="background-image: url(<c:url value="/resources/images/bg2_new.jpg"/>); background-size: 100%;">


</div>

<nav class="navbar navbar-inverse" style="color: white; background: #031612">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <%
        String url = request.getRequestURI().toString();
        %>


        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">

                <li <%if(url.endsWith("currentLocation.jsp")){%>class="active"<%}%> ><a href="currentLocation?soldier=all&currentLoc=true"><b>HOME</b></a></li>


                <li class="dropdown <%if(url.endsWith("addSoldier.jsp")||url.endsWith("soldier.jsp")){%>active<%}%>"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><b>SOLDIERS</b><span class="caret"></span></a>
                    <ul class="dropdown-menu col-sm-2" style="background: #031612">
                        <li><a href="addSoldier" style="color: white;"><b>&nbsp;&nbsp;ADD NEW</b>&nbsp;&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-plus"></span></a></li>
                        <li class="divider"></li>
                        <li><a href="soldier" style="color: white;"><b>&nbsp;&nbsp;SHOW ALL</b>&nbsp;&nbsp;<span class="glyphicon glyphicon-list"></span></a></li>
                    </ul>
                </li>

                <li class="dropdown <%if(url.endsWith("addPillar.jsp")||url.endsWith("pillar.jsp")){%>active<%}%>"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><b>PILLARS</b><span class="caret"></span></a>
                    <ul class="dropdown-menu col-sm-2" style="background: #031612">
                        <li><a href="addPillar" style="color: white;"><b>&nbsp;&nbsp;ADD NEW</b>&nbsp;&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-plus"></span></a></li>
                        <li class="divider"></li>
                        <li><a href="pillar" style="color: white;"><b>&nbsp;&nbsp;SHOW ALL</b>&nbsp;&nbsp;<span class="glyphicon glyphicon-list"></span></a></li>
                    </ul>
                </li>

                <%--<li <%if(url.endsWith("home.jsp")){%>class="active"<%}%> ><a href="home"><b>RECENT CAPTURES</b></a></li>--%>
                <%--<li <%if(url.endsWith("reports.jsp")){%>class="active"<%}%> ><a href="reports"><b>REPORTS</b></a></li>--%>
                <li <%if(url.endsWith("customSearch.jsp")){%>class="active"<%}%> ><a href="customSearch"><b>CUSTOM SEARCH</b></a></li>
                <li <%if(url.endsWith("specialOperations.jsp")){%>class="active"<%}%> ><a href="specialOperations"><b>SPECIAL OPERATIONS</b></a></li>
            </ul>
            <br>

        </div>
    </div>
</nav>


        <nav class="navbar navbar-inverse" style="color: white; background: #a8a6a1">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar2">
                        <span class="glyphicon glyphicon-triangle-bottom"></span>
                    </button>
                </div>


                <div class="collapse navbar-collapse" id="myNavbar2">
                    <ul class="nav navbar-nav">
                        <form action="pillar" method="get" class="navbar-form navbar-right" role="search">
                            <div class="form-group input-group">
                                <input type="text" name="findpillar" class="form-control" placeholder="Search Pillar"
                                       required="">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" type="submit">
                                      <span class="glyphicon glyphicon-search"></span>
                                    </button>
                                </span>
                            </div>
                        </form>
                        <form action="soldier" method="get" class="navbar-form navbar-right" role="search">
                            <div class="form-group input-group">
                                <input type="text" name="findsoldier" class="form-control" placeholder="Search Soldier"
                                       required="">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary" type="submit">
                                      <span class="glyphicon glyphicon-search"></span>
                                    </button>
                                </span>
                            </div>
                        </form>
                    </ul>
                    <%--<ul class="nav navbar-nav navbar-right">
                        <%
                            if (session.getAttribute("userId") != null) {
                        %>
                        <form action="login" method="post">
                            <li><span
                                    class="glyphicon glyphicon-user">Logged in as <%=session.getAttribute("username")%></span>
                                <input name="logout" type="hidden" value="logout">
                                <button type="submit" class="btn btn-danger">Logout</button>
                            </li>
                        </form>
                        <%
                        } else {
                        %>
                        <form action="home" method="post">
                            <li></li>
                            <li><input name="username" type="text" required="" placeholder="username"
                                       style="color: black"/><span class="glyphicon glyphicon-user"></span>
                                <input name="password" type="password" required="" placeholder="password"
                                       style="color: black"/><span
                                        class="glyphicon glyphicon-user"></span>
                                <input type="submit" value="insert" style="color: black"/>
                            </li>
                        </form>
                        <%}%>
                    </ul>--%>

                    <%--<ul class="nav navbar-nav navbar-right">
                        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                    </ul>--%>

                    <ul class="nav navbar-nav navbar-right">
                        <%
                            if (session.getAttribute("userId") != null) {
                        %>
                        <li>
                            <form action="login" method="post">
                            <span class="glyphicon glyphicon-user"></span>&nbsp;<span style="color: #031612"><b>logged in as <%=session.getAttribute("username")%></b></b></span>
                                <input name="logout" type="hidden" value="logout">
                                <button type="submit" class="btn btn-danger navbar-btn"><span class="glyphicon glyphicon-log-out"></span> Log out</button>
                            </form>
                        </li>
                        <%
                        } else {
                        %>
                        <form action="home" method="post">
                            <li></li>
                            <li><input name="username" type="text" required="" placeholder="username"
                                       style="color: black"/><span class="glyphicon glyphicon-user"></span>
                                <input name="password" type="password" required="" placeholder="password"
                                       style="color: black"/><span
                                        class="glyphicon glyphicon-user"></span>
                                <input type="submit" value="insert" style="color: black"/>
                            </li>
                        </form>
                        <%}%>
                    </ul>

                </div>

            </div>
        </nav>







