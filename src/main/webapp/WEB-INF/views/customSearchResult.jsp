<%-- 
    Document   : customSearchResult
    Created on : Feb 11, 2017, 2:36:55 AM
    Author     : Al-Amin
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="net.bgb.monitoring.connection.Database"%>
<%@ page import="net.bgb.monitoring.web.util.StringConversion" %>
<%@ page import="java.util.List" %>
<%@ page import="java.io.File" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Collections" %>
<%@ page import="net.bgb.monitoring.service.RoleService" %>
<%@ include file="header.jsp" %>

<h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Search Results(sorted by Newer to Older):</h3><br><br>
<%
    
             Database db = new Database();
                db.connect();
                try{


                    List<String> imageUrlList = new ArrayList<>();
                    List<String> imageUrlListTemp = new ArrayList<>();
                    int iter = 0;
                    int viewIter = 0;

                    File imageDir = new File("C:\\Users\\Al-Amin\\Documents\\NetBeansProjects\\BGBSpring\\src\\main\\webapp\\resources\\images\\pillarImg");
                    for (File imageFile : imageDir.listFiles()) {
                        String imageFileName = imageFile.getName();

                        // add this images name to the list we are building up
                        int ii=imageFileName.split("_")[0].length();

                        imageFileName=imageFileName.substring(ii,imageFileName.length())+"#"+imageFileName.substring(0,ii);
                        imageUrlListTemp.add(imageFileName);

                    }

                    if (imageUrlListTemp.size() > 0) {
                        Collections.sort(imageUrlListTemp.subList(1, imageUrlListTemp.size()));
                    }

                    for(int jj=0;jj<imageUrlListTemp.size();jj++) {
                        imageUrlList.add( imageUrlListTemp.get(jj).split("#")[1]+imageUrlListTemp.get(jj).split("#")[0]);
                    }

                    for (iter = imageUrlList.size()-1; iter >=0; iter--) {
                        String imgUrl = "/resources/images/pillarImg/" + imageUrlList.get(iter);


                        Statement st = db.connection.createStatement();
                        String query = "select * from pillar where id="+imageUrlList.get(iter).split("_")[0];
                        ResultSet rsPillar = st.executeQuery(query);
                        rsPillar.next();

                        Statement st2 = db.connection.createStatement();
                        String qry2 = "select * from pillar_updated_by where img_url like '"+imageUrlList.get(iter)+"'";
                        ResultSet rs2PillarUpdatedBy = st2.executeQuery(qry2);


                        if((!rs2PillarUpdatedBy.next()) || rs2PillarUpdatedBy.getString("soldier_id")==null || rs2PillarUpdatedBy.getString("soldier_id").equals("null")) {
                            continue;
                        }

                        Statement st3 = db.connection.createStatement();
                        String qry3 = "select * from soldier where id="+rs2PillarUpdatedBy.getString("soldier_id");
                        ResultSet rs3Soldier = st3.executeQuery(qry3);
                        rs3Soldier.next();




                        if(request.getParameter("soldierId")!=null && request.getParameter("soldierId").length()>0 && !request.getParameter("soldierId").equals(rs2PillarUpdatedBy.getString("soldier_id"))){
                            continue;
                        }

                        if(request.getParameter("pillarId")!=null && request.getParameter("pillarId").length()>0 && !request.getParameter("pillarId").equals(imageUrlList.get(iter).split("_")[0])){
                            continue;
                        }

                        if(request.getParameter("fromDate")!=null && request.getParameter("fromDate").length()>0){
                            String from = request.getParameter("fromDate").toString().split("-")[0]+request.getParameter("fromDate").toString().split("-")[1]+request.getParameter("fromDate").toString().split("-")[2];
                            if(request.getParameter("fromTime")!=null && request.getParameter("fromTime").length()>0){
                                from+=request.getParameter("fromTime").substring(0, 2)+request.getParameter("fromTime").substring(request.getParameter("fromTime").length()-2, request.getParameter("fromTime").length());
                            }
                            else{
                                from+="0000";
                            }
                            if(from.compareTo(imageUrlList.get(iter).split("_")[1]+imageUrlList.get(iter).split("_")[2]+imageUrlList.get(iter).split("_")[3]+imageUrlList.get(iter).split("_")[4]+imageUrlList.get(iter).split("_")[5])>0) {
                                continue;
                            }
                        }

                        if(request.getParameter("toDate")!=null && request.getParameter("toDate").length()>0){
                            String to = request.getParameter("toDate").toString().split("-")[0]+request.getParameter("toDate").toString().split("-")[1]+request.getParameter("toDate").toString().split("-")[2];
                            if(request.getParameter("toTime")!=null && request.getParameter("toTime").length()>0){
                                to+=request.getParameter("toTime").substring(0, 2)+request.getParameter("toTime").substring(request.getParameter("toTime").length()-2, request.getParameter("toTime").length());
                            }
                            else{
                                to+="0000";
                            }

                            if(to.compareTo(imageUrlList.get(iter).split("_")[1]+imageUrlList.get(iter).split("_")[2]+imageUrlList.get(iter).split("_")[3]+imageUrlList.get(iter).split("_")[4]+imageUrlList.get(iter).split("_")[5])<0) {
                                continue;
                            }
                        }

                        if(!RoleService.isViewAuthorized(session.getAttribute("username").toString(),rs3Soldier.getString("battalion"))) {
                            continue;
                        }
                        if(!RoleService.isViewAuthorized(session.getAttribute("username").toString(),rsPillar.getString("battalion"))) {
                            continue;
                        }


if((viewIter%3)==0) {
%>
<div class="container">
  <div class="row">
<%}%>
    <div class="col-sm-4">
      <div class="panel panel-primary">
        <div class="panel-heading">Pillar Name: <%=StringConversion.nullReplace(rsPillar.getString("name"), Constants.NOT_AVAILABLE)%></div>
        <div class="panel-heading">Pillar Number: <%=StringConversion.nullReplace(rsPillar.getString("number"), Constants.NOT_AVAILABLE)%></div>
        <div class="panel-heading">Current Status: <%=StringConversion.nullReplace(imageUrlList.get(iter).split("_")[7], Constants.NOT_AVAILABLE)%></div>
        <div class="panel-heading">Captured By: <%=StringConversion.nullReplace(rs3Soldier.getString("name"), Constants.NOT_AVAILABLE)+" ("+StringConversion.nullReplace(rs3Soldier.getString("username"), Constants.NOT_AVAILABLE)+")"%></div>
        <div class="panel-heading">Captured At: <%=StringConversion.nullReplace(imageUrlList.get(iter).split("_")[3]+"/"+imageUrlList.get(iter).split("_")[2]+"/"+imageUrlList.get(iter).split("_")[1]+" "+imageUrlList.get(iter).split("_")[4]+":"+imageUrlList.get(iter).split("_")[5], Constants.NOT_AVAILABLE)%></div>
        <div class="panel-body"><img src=
                                     <c:url value="<%=imgUrl%>"/> class="img-responsive"
                                     style="width:100%; height: 300px" alt="Image"></div>
        <div class="panel-footer">Longitude: <%=StringConversion.nullReplace(rsPillar.getString("longitude"), Constants.NOT_AVAILABLE)%></div>
        <div class="panel-footer">Latitude: <%=StringConversion.nullReplace(rsPillar.getString("latitude"), Constants.NOT_AVAILABLE)%></div>
      </div>
    </div>
    
 <%
if(((viewIter+1)%3)==0) {
%>
  </div>
</div><br>
<%}%>

<%
    viewIter++;
}
if((viewIter%3)!=0){
%>
</div>
</div><br>
<%}if(viewIter==0){%>
<legend style="margin-left: 300px;">Nothing Found</legend><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<%}
    }catch (Exception e){
        response.sendRedirect("somethingWentWrong");
    }finally {
        db.close();
    }


%>
<%@ include file="footer.jsp" %>

