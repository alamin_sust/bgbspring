<%--
    Document   : home
    Created on : Feb 7, 2017, 12:19:15 AM
    Author     : Al-Amin
--%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="net.bgb.monitoring.connection.Database" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.io.File" %>
<%@ page import="java.util.Collections" %>
<%@ page import="net.bgb.monitoring.web.util.Constants" %>
<%@ page import="net.bgb.monitoring.web.util.StringConversion" %>
<%@ page import="net.bgb.monitoring.service.RoleService" %>

<%@ include file="header.jsp" %>

<%

    Database db = new Database();
    db.connect();
try{

%>


<div class="container text-center">
    <div class="row">
        <%--<div class="col-sm-4 well">
            <div class="well" style="color: red">
                <h3><b>Reports</b></h3>
            </div>--%>

            <%
                int reportsToShow = Constants.REPORTS_TO_SHOW;
                Statement st4 = db.connection.createStatement();
                String qry4 = "select * from report_to_bgb order by datetime desc LIMIT 0, "+reportsToShow;
                ResultSet rsReport  = st4.executeQuery(qry4);

                Statement st5 = db.connection.createStatement();
                String qry5 = "select count(*) as cnt from report_to_bgb";
                ResultSet rsReportCount  = st5.executeQuery(qry5);
                rsReportCount.next();


            while (rsReport.next()) {
            %>
            <%--<div class="col-sm-13">
                <div class="well">
                    <p><strong>Time of Report:</strong> <b style="color: red"><%=StringConversion.nullReplace(rsReport.getString("datetime"),Constants.NOT_AVAILABLE)%></b></p>
                    <p><strong>Name: </strong><b style="color: green"><%=StringConversion.nullReplace(rsReport.getString("name"),Constants.NOT_PROVIDED)%></b> </p>
                    <p><strong>Mobile No.:</strong><b style="color: green"> <%=StringConversion.nullReplace(rsReport.getString("mobile"),Constants.NOT_PROVIDED)%></b> </p>
                    <p><strong>Address:</strong><b style="color: green"> <%=StringConversion.nullReplace(rsReport.getString("address"),Constants.NOT_PROVIDED)%></b> </p>
                    <p><strong>Message:</strong></p>
                    <p style="color: green"><strong> <%=StringConversion.nullReplace(rsReport.getString("message"),Constants.NOT_PROVIDED)%></strong> </p>
            </div>
            </div>--%>

            <%}if(rsReportCount.getInt("cnt")>reportsToShow){%>
            <%--<p><a href="reports"><b>All Reports</b></a></p>--%>
            <%}%>

        <%--</div>--%>
        <div class="col-sm-7 col-sm-offset-3">
            <h2>Recent Captures</h2>




            <%

                List<String> imageUrlListTemp = new ArrayList<>();
                List<String> imageUrlList = new ArrayList<>();
                int iter = 0;
                File imageDir = new File("C:\\Users\\Al-Amin\\Documents\\NetBeansProjects\\BGBSpring\\src\\main\\webapp\\resources\\images\\pillarImg");
                for (File imageFile : imageDir.listFiles()) {
                    String imageFileName = imageFile.getName();

                    // add this images name to the list we are building up
                    int ii=imageFileName.split("_")[0].length();

                    imageFileName=imageFileName.substring(ii,imageFileName.length())+"#"+imageFileName.substring(0,ii);
                        imageUrlListTemp.add(imageFileName);

                }

                if (imageUrlListTemp.size() > 0) {
                    Collections.sort(imageUrlListTemp.subList(1, imageUrlListTemp.size()));
                }

                for(int jj=0;jj<imageUrlListTemp.size();jj++) {
                    imageUrlList.add( imageUrlListTemp.get(jj).split("#")[1]+imageUrlListTemp.get(jj).split("#")[0]);
                }

                int j;
                for (iter = imageUrlList.size()-1, j=0;j<reportsToShow && iter >=0; iter--) {
                    String imgUrl = "/resources/images/pillarImg/" + imageUrlList.get(iter);


                    Statement st = db.connection.createStatement();
                    String qry = "select * from pillar where id="+imageUrlList.get(iter).split("_")[0];
                    ResultSet rsPillar = st.executeQuery(qry);
                    rsPillar.next();

                    Statement st2 = db.connection.createStatement();
                    String qry2 = "select * from pillar_updated_by where img_url like '"+imageUrlList.get(iter)+"'";
                    ResultSet rs2PillarUpdatedBy = st2.executeQuery(qry2);


                    if((!rs2PillarUpdatedBy.next()) || rs2PillarUpdatedBy.getString("soldier_id")==null || rs2PillarUpdatedBy.getString("soldier_id").equals("null")) {
                        continue;
                    }

                    Statement st3 = db.connection.createStatement();
                    String qry3 = "select * from soldier where id="+rs2PillarUpdatedBy.getString("soldier_id");
                    ResultSet rs3Soldier = st3.executeQuery(qry3);
                    rs3Soldier.next();


                    if(!RoleService.isViewAuthorized(session.getAttribute("username").toString(),rs3Soldier.getString("battalion"))) {
                        continue;
                    }
                    if(!RoleService.isViewAuthorized(session.getAttribute("username").toString(),rsPillar.getString("battalion"))) {
                        continue;
                    }


                    j++;




            %>




            <div class="row">
                <div class="col-sm-3">
                    <div class="well">
                        <p>Captured By:</p>
                        <p><strong><%=StringConversion.nullReplace(rs3Soldier.getString("name"),Constants.NOT_AVAILABLE)%></strong></p>
                        <p>Designation: <strong><%=StringConversion.nullReplace(rs3Soldier.getString("designation"),Constants.NOT_AVAILABLE)%></strong></p>
                        <p>Date Captured: <strong style="color: darkred"><%=StringConversion.nullReplace(imageUrlList.get(iter).split("_")[3]+"/"+imageUrlList.get(iter).split("_")[2]+"/"+imageUrlList.get(iter).split("_")[1], Constants.NOT_AVAILABLE)%></strong></p>
                        <p>Time Captured: <strong style="color: darkgreen;"><%=StringConversion.nullReplace(imageUrlList.get(iter).split("_")[4]+":"+imageUrlList.get(iter).split("_")[5]+":"+imageUrlList.get(iter).split("_")[6], Constants.NOT_AVAILABLE)%></strong></p>

                        <img src=<c:url value="/resources/images/soldier.jpg"/> class="img-circle" height="55" width="55" alt="Avatar">
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="well">
                        <h4>Pillar Id: <strong><%=StringConversion.nullReplace(imageUrlList.get(iter).split("_")[0],Constants.NOT_AVAILABLE)%></strong></h4>
                        <h4>Name: <strong><%=StringConversion.nullReplace(rsPillar.getString("name"),Constants.NOT_AVAILABLE)%></strong></h4>
                        <h4>Pillar Number: <strong><%=StringConversion.nullReplace(rsPillar.getString("number"),Constants.NOT_AVAILABLE)%></strong></h4>

                        <h4>Status: <strong style="color: blue"><%=StringConversion.nullReplace(imageUrlList.get(iter).split("_")[7],Constants.NOT_AVAILABLE)%></strong></h4>
                        <p><img src=<c:url value="<%=imgUrl%>"/> height="130" width="150" alt="Avatar"></p>
                    </div>
                </div>
            </div>
            <%
                }
            %>
        </div>


        <%--<div class="col-sm-2 well">
            <div class="thumbnail">
                <p>Upcoming Events:</p>
                <img src=<c:url value="/resources/images/soldier.jpg"/> alt="Paris" width="400" height="300">
                <p><strong>Sylhet</strong></p>
                <p>Friday, 17 February 2017</p>
                <button class="btn btn-primary">Info</button>
            </div>
            <div class="well">
                <p>ADS</p>
            </div>
            <div class="well">
                <p>ADS</p>
            </div>
        </div>--%>
    </div>
</div>
<%
    }catch (Exception e){
        response.sendRedirect("somethingWentWrong");
    }finally {
    db.close();
    }


%>

<%@ include file="footer.jsp" %>