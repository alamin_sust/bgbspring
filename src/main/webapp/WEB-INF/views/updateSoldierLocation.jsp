<%-- 
    Document   : updateSoldierLocation
    Created on : Feb 25, 2017, 9:36:59 PM
    Author     : Al-Amin
--%>

<%@page import="java.sql.Date"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.nio.file.Paths"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="java.io.File"%>
<%@page import="java.io.DataInputStream"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="net.bgb.monitoring.connection.Database"%>
<%@ page import="net.bgb.monitoring.web.util.ConverterUtil" %>

<%
   // Returns all employees (active and terminated) as json.
    Database db = new Database();
    db.connect();
try{

               Statement st2 = db.connection.createStatement();
                String mxIdQry = "select max(id)+1 as mxid from soldier_location";
                ResultSet rs2 = st2.executeQuery(mxIdQry);
  
                rs2.next();



                Statement st3 = db.connection.createStatement();
                String mxPatrolIdQry = "select max(patrol_id)+1 as mxpatrolid from soldier_location";
                ResultSet rs3 = st3.executeQuery(mxPatrolIdQry);

                rs3.next();

    String patrolId = new String();

    if(request.getParameter("patrolId")==null) {
        patrolId = rs3.getString("mxpatrolid");
        Statement st4 = db.connection.createStatement();
        String qry4 = "UPDATE soldier set patrolling_status=1 where id="+request.getParameter("id");

        st4.executeUpdate(qry4);
    }
    else {
        patrolId = request.getParameter("patrolId");
    }




    Statement st = db.connection.createStatement();
                String qry = "insert into soldier_location(id,soldier_id,longitude,latitude,datetime,patrol_id,battalion,imie_number) values("+rs2.getString("mxid")+","+request.getParameter("id")
                        +", '"+request.getParameter("longitude")+"','"+request.getParameter("latitude")+"',now(),"+patrolId+",'"+ ConverterUtil.getBattalionFromImie(request.getParameter("authImie"))+"','"+request.getParameter("authImie")+"')";
               

                    if(request.getParameter("endPatrol")!=null) {
                        Statement st4 = db.connection.createStatement();
                        String qry4 = "UPDATE soldier set patrolling_status=0 where id="+request.getParameter("id");
                        st4.executeUpdate(qry4);
                    }
                    st.executeUpdate(qry);
%>
{"result":"1",
"patrolId":"<%=patrolId%>"
}
<%
                }
                catch(Exception e){
                    %>
{"result":"0"}
<%
                } finally {
    db.close();
    }


%>
