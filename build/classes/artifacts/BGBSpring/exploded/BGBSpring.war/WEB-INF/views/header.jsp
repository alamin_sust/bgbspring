<%--
  Created by IntelliJ IDEA.
  User: Al-Amin
  Date: 3/8/2017
  Time: 11:05 PM
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>BGB</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        /* Set black background color, white text and some padding */
        footer {
            background-color: #555;
            color: white;
            padding: 15px;
        }
        .navbar {
            margin-bottom: 50px;
            border-radius: 0;
        }

        /* Remove the jumbotron's default bottom margin */
        .jumbotron {
            margin-bottom: 0;
        }

        /* Add a gray background color and some padding to the footer */
        footer {
            background-color: #f2f2f2;
            padding: 25px;
        }
    </style>
</head>
<body>

    <%

%>


<div class="jumbotron" style="background-image: url(../../resources/images/bg2.jpg); background-size: 100%;">
    <div class="container text-center">
        <img src="../../resources/images/logo-bgb.jfif" class="img-circle" height="105" width="105" alt="Avatar"/>
        <b><h2 style="color: white">BGB Patrol Monitoring & Border Condition Reporting</h2></b>
    </div>
    <form action="pillar" method="get" class="navbar-form navbar-right" role="search">
        <div class="form-group input-group">
            <input type="text" name="findpillar" class="form-control" placeholder="Search Pillar" required="">
            <span class="input-group-btn">
            <button class="btn btn-default" type="submit">
              <span class="glyphicon glyphicon-search"></span>
            </button>
          </span>
        </div>
    </form>
    <form action="soldier" method="get" class="navbar-form navbar-right" role="search">
        <div class="form-group input-group">
            <input type="text" name="findsoldier" class="form-control" placeholder="Search Soldier" required="">
            <span class="input-group-btn">
            <button class="btn btn-default" type="submit">
              <span class="glyphicon glyphicon-search"></span>
            </button>
          </span>
        </div>
    </form>
</div>

<nav class="navbar navbar-inverse" style="color: white; background: darkkhaki">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar"  >
            <ul class="nav navbar-nav" >
                <li><a href="home" style="color: lightyellow"><b>Home</b></a></li>
                <li><a href="soldier" style="color: lightyellow"><b>Soldiers</b></a></li>
                <li><a href="pillar" style="color: lightyellow"><b>Pillars</b></a></li>
                <li><a href="addSoldier" style="color: lightyellow"><b>Add a New Soldier</b></a></li>
                <li class="active"><a href="addPillar" style="color: lightyellow"><b>Add a New Pillar</b></a></li>
                <li><a href="currentLocation?soldier=all&currentLoc=true" style="color: lightyellow"><b>Soldier's Current Location</b></a></li>
                <li><a href="customSearch" style="color: lightyellow"><b>Customized Search</b></a></li>
            </ul>
            <br>
            <ul class="nav navbar-nav navbar-right">
                <%
                    if(session.getAttribute("userId")!=null) {
                %>
                <form action="home" method="post">
                    <li><span class="glyphicon glyphicon-user">Logged in as <%=session.getAttribute("username")%></span>
                        <input name="logout" type="hidden" value="logout">
                        <button type="submit" class="btn btn-danger">Logout</button>
                    </li>
                </form>
                <%
                }else{
                %>
                <form action="home" method="post">
                    <li></li>
                    <li><input name="username" type="text" required="" placeholder="username" style="color: black"/><span class="glyphicon glyphicon-user"></span>
                        <input name="password" type="password" required="" placeholder="password" style="color: black"/><span class="glyphicon glyphicon-user"></span>
                        <input type="submit" value="insert" style="color: black"/>
                    </li>
                </form>
                <%}%>
            </ul>
        </div>
    </div>
</nav>

