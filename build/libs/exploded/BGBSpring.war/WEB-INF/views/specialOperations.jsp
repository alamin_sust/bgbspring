<%--
    Document   : pillar
    Created on : Feb 9, 2017, 2:01:35 AM
    Author     : Al-Amin
--%>
<%@page import="net.bgb.monitoring.connection.Database" %>
<%@page import="java.io.File" %>
<%@page import="java.sql.ResultSet" %>
<%@page import="java.sql.Statement" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.Collections" %>
<%@page import="java.util.List" %>
<%@ page import="net.bgb.monitoring.web.util.Constants" %>
<%@ page import="net.bgb.monitoring.web.util.StringConversion" %>
<%@ page import="net.bgb.monitoring.service.RoleService" %>
<%@ include file="header.jsp" %>

<%

    Database db = new Database();
    db.connect();
    try{
        int iter = 0;
        String query = "select * from pillar where id>="+ Constants.SPECIAL_OPERATIONS_STARTING_INDEX+" and id<="+Constants.SPECIAL_OPERATIONS_ENDING_INDEX;


        Statement st = db.connection.createStatement();
        ResultSet rs = st.executeQuery(query);

        for (iter = 0; rs.next();) {
%>
<h3><div class="well" style="align-content: center;">Special Operations</div></h3>
<%

    List<String> imageUrlList = new ArrayList<>();

    File imageDir = new File("C:\\Users\\Al-Amin\\Documents\\NetBeansProjects\\BGBSpring\\src\\main\\webapp\\resources\\images\\pillarImg");
    for (File imageFile : imageDir.listFiles()) {
        String imageFileName = imageFile.getName();

        if (rs.getString("id") != null && rs.getString("id").equals(imageFileName.split("_")[0]) ) {
            imageUrlList.add(imageFileName);
        }

    }
    String imgUrl = "/resources/images/pillar.jpg";
    if (imageUrlList.size() > 0) {
        Collections.sort(imageUrlList.subList(1, imageUrlList.size()));
        imgUrl = "/resources/images/pillarImg/" + imageUrlList.get(imageUrlList.size() - 1);
    }


    if ((iter % 3) == 0) {
%>
<div class="container">
    <div class="row">
        <%}%>
        <div class="col-sm-4">
            <div class="panel panel-primary">
                <div class="panel-heading"><%=StringConversion.nullReplace(rs.getString("name"),Constants.NOT_AVAILABLE)%>
                </div>
                <div class="panel-body"><img src=
                                             <c:url value="<%=imgUrl%>"/> class="img-responsive"
                                             style="width:100%; height: 300px" alt="Image"></div>

                <div class="panel-footer">Current Status: <strong style="color: blue"><%=StringConversion.nullReplace(rs.getString("situation"),Constants.NOT_AVAILABLE)%></strong>
                </div>
                <div class="panel-footer">Longitude: <strong><%=StringConversion.nullReplace(rs.getString("longitude"),Constants.NOT_AVAILABLE)%></strong>
                </div>
                <div class="panel-footer">Latitude: <strong><%=StringConversion.nullReplace(rs.getString("latitude"),Constants.NOT_AVAILABLE)%></strong>
                </div>
                <form action="specialOperationsHistory" method="get">
                    <input type="hidden" name="id" value='<%=rs.getString("id")%>'><br>
                    <input type="hidden" name="name" value='<%=rs.getString("name")%>'><br>

                    <button type="submit" class="btn btn-danger">Operation History</button>
                </form>
            </div>
        </div>

        <%
            if (((iter + 1) % 3) == 0) {
        %>
    </div>
</div>
<br>
<%}%>

<%iter++;
}
    if ((iter % 3) != 0) {
%>
</div>
</div><br>
<%}if(iter==0){%>
<legend style="margin-left: 300px;">Nothing Found</legend><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<%}
}catch (Exception e){
    response.sendRedirect("somethingWentWrong");
}finally {
    db.close();
}


%>
<%@ include file="footer.jsp" %>

