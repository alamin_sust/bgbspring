<%@page import="java.util.Random"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="net.bgb.monitoring.connection.Database"%>
<%@ page import="net.bgb.monitoring.web.util.EncryptionUtil" %>
<%@ include file="header.jsp" %>

<%
    Database db = new Database();
    db.connect();
    try{
    
if(request.getParameter("name")!=null  && request.getParameter("battalion")!=null && !request.getParameter("name").isEmpty() && !request.getParameter("battalion").isEmpty()) {
    

                Statement st = db.connection.createStatement();
                String mxIdQry = "select max(id)+1 as mxid from soldier";
                ResultSet rs = st.executeQuery(mxIdQry);
  
                rs.next();
                String id = rs.getString("mxid");
                String name=request.getParameter("name");
                String designation  = request.getParameter("designation");
                String solUser = request.getParameter("solUser");
                String solPass = request.getParameter("solPass");
                String battalion = request.getParameter("battalion");

                solPass = EncryptionUtil.generateSecuredHash(solPass);

                //for new soldier insertion
                String query = "insert into soldier(id,name,designation,username,password,battalion) values("+id+",'"+name+"', '"+designation+"','"+solUser+"','"+solPass+"','"+battalion+"')";

                //for password reset
                //String query = "update soldier set password  = '"+solPass+"' where username like '"+solUser+"'";



    Statement stIns = db.connection.createStatement();
                stIns.executeUpdate(query);
                
                
                
}
String passGen = new String();
                
                Random rand = new Random();
                
                for(int i=0;i<4;i++)
                {
                    int key;
                    while((key = rand.nextInt())<0){}
                    key%=62;
                    if(key<26){
                        passGen+=(char)('A'+key);
                    }
                    else if(key<52) {
                        passGen+=(char)('a'+key-26);
                    }
                    else{
                        passGen+=(char)('0'+key-52);
                    }
                }

%>    
    

  <%
      if(request.getParameter("name")!=null && request.getParameter("battalion")!=null && !request.getParameter("name").isEmpty() && !request.getParameter("battalion").isEmpty()){
          %>
<div class="container-fluid text-center">  
     <div class="alert alert-success fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">X</a>
        <p><strong>Success!</strong></p>
        Soldier Inserted Into Central Database Successfully.
      </div>
    <%
        request.setAttribute("name", null);
        
    }%>
  <div class="row content">
    <div class="col-sm-2 sidenav">
    </div>
    <div class="col-sm-8 text-left"> 
      <legend style="font-size: 35px">Add a New Soldier</legend>
      <br>
      <form class="form-inline" method="post" action="addSoldier">
      Name:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input type="text" name="name" class="form-control" size="50" placeholder="Name" required>
      <br><br>
      Designation: <input type="text" name="designation" class="form-control" size="50" placeholder="Designation" required>
      <br><br>
      Username: <input type="text" name="solUser" class="form-control" size="50" placeholder="Username" required>
      <br><br>
      <input type="hidden" name="solPass" value=<%=passGen%>>
      Password(auto generated): <b><%=passGen%></b>
          <br><br>
          Battalion: <input type="text" name="battalion" class="form-control" size="50" placeholder="Battalion" required>
      <br><br>
      <button type="submit" class="btn btn-danger">Insert</button>
      </form>
    </div>
    <div class="col-sm-2 sidenav">
      
    </div>
  </div>
</div>
<br><br><br><br><br><br><br>
<%
    }catch (Exception e){
        response.sendRedirect("somethingWentWrong");
    }finally {
    db.close();
    }


%>
<%@ include file="footer.jsp" %>


