<%-- 
    Document   : currentLocation
    Created on : Feb 25, 2017, 2:17:08 AM
    Author     : Al-Amin
--%>

<%@page import="net.bgb.monitoring.connection.Database" %>
<%@page import="java.sql.ResultSet" %>
<%@page import="java.sql.Statement" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.ArrayList" %>
<%@ page import="net.bgb.monitoring.web.util.Constants" %>
<%@ page import="net.bgb.monitoring.service.RoleService" %>
<%@ page import="java.sql.Struct" %>

<%@ include file="header.jsp" %>

<div id="fulpage">

    <script>
        function SelectElement(valueToSelect) {
            var element = document.getElementById('soldier');
            element.value = valueToSelect;
            var element2 = document.getElementById('soldier2');
            element2.value = valueToSelect;
        }
    </script>
    <script src="http://maps.google.com/maps/api/js?sensor=false"
            type="text/javascript"></script>

    <%
        String formattedDate = new String();
        int patrollingStatus = 0;
        String searchCriteriaStr=new String();
        Database db = new Database();
        db.connect();
        try{

            if(request.getParameter("currentLoc")!=null && request.getParameter("currentLoc").equals("false") && session.getAttribute("viewSoldier")==null) {
                session.setAttribute("viewSoldier","all");
            }



            searchCriteriaStr+="Serach Results";
        //BOP positioning
        Statement stPillar = db.connection.createStatement();
        String qryPillar = "select * from pillar where id>="+ Constants.PILLAR_STARTING_INDEX + " and longitude is not null and latitude is not null and longitude LIKE '%.%' and latitude LIKE '%.%' ";
        ResultSet rsPillar = stPillar.executeQuery(qryPillar);

        Statement st = db.connection.createStatement();
        String qry = "select * from soldier_location sl where sl.id>0";


            //view Level Select Button
            if(request.getParameter("viewCriteria")!=null && !request.getParameter("viewCriteria").isEmpty()) {

                int viewLevel = Integer.parseInt(request.getParameter("viewCriteria").split("_")[0]);

                Statement viewSt = db.connection.createStatement();
                String viewQry = "select * from soldier where username like '"+request.getParameter("viewCriteria").substring(request.getParameter("viewCriteria").split("_")[0].length()+1)+"'";
                ResultSet viewRs = viewSt.executeQuery(viewQry);
                viewRs.next();


                if(viewLevel == 6) {
                    qry += " and sl.soldier_id=" + viewRs.getString("id");
                    searchCriteriaStr+=" for Soldier ID:"+viewRs.getString("id");
                    session.setAttribute("viewSoldier",viewRs.getString("id"));
                } else {
                    session.setAttribute("viewUsername",request.getParameter("viewCriteria").substring(request.getParameter("viewCriteria").split("_")[0].length()+1));
                    session.setAttribute("viewSoldier",null);
                }

               /* if (request.getParameter("soldier") != null && !request.getParameter("soldier").equals("all")) {


                }*/

            }






        /*if (request.getParameter("soldier") != null && !request.getParameter("soldier").equals("all")) {
            qry += " and sl.soldier_id=" + request.getParameter("soldier");
            searchCriteriaStr+=" for Soldier ID:"+request.getParameter("soldier");

        }*/

            if (request.getParameter("viewCriteria")==null && session.getAttribute("viewSoldier") != null && !session.getAttribute("viewSoldier").toString().equals("all")) {
                qry += " and sl.soldier_id=" + session.getAttribute("viewSoldier").toString();
                searchCriteriaStr+=" for Soldier ID:"+session.getAttribute("viewSoldier").toString();

            }




        if (request.getParameter("date") != null && request.getParameter("date").length()>0) {
            qry += " and sl.datetime like '" + request.getParameter("date") + "%'";
            searchCriteriaStr+=" for Date:"+ request.getParameter("date");
        }
        if (request.getParameter("currentLoc") != null && request.getParameter("currentLoc").equals("true")) {
            qry += " and sl.datetime >=(select MAX(datetime) from soldier_location sl2 where sl.soldier_id=sl2.soldier_id)";
        }

        if(request.getParameter("currentLoc") != null && request.getParameter("currentLoc").equals("true")&& session.getAttribute("viewSoldiers")!=null) {
                qry+=" and sl.username like '"+session.getAttribute("viewSoldiers").toString()+"'";
        }


        qry += " and sl.battalion is not null and id>=(select max(id)-1000 from soldier_location sl2) order by sl.soldier_id asc,sl.datetime desc";

         /*   qry+=" limit 1000";*/

        ResultSet rs = st.executeQuery(qry);

        int threeColor = 0;
        if (session.getAttribute("viewSoldier") != null && !session.getAttribute("viewSoldier").toString().equals("all")
                && (request.getParameter("currentLoc") == null||request.getParameter("currentLoc").equals("false"))) {
            threeColor = 1;
        }


        ArrayList<String> names = new ArrayList<>();
        ArrayList<String> locs = new ArrayList<>();
        ArrayList<String> patrolId = new ArrayList<>();

/*names.add("'Consteble Jubayer'");
locs.add(24.9156412);
locs.add(91.952099);
names.add("'Habildar Tanvir'");
locs.add(24.9161319);
locs.add(91.9533794);
names.add("'Night Guard Shapan'");
locs.add(24.9168021);
locs.add(91.9547929);
*/

        ArrayList<Integer> currentLocation = new ArrayList<>();
        while (rs.next()) {
            if(!RoleService.isViewAuthorized(session.getAttribute("viewUsername").toString(),rs.getString("battalion"))) {
                continue;
            }


            Statement st2 = db.connection.createStatement();
            String qry2 = "select * from soldier where id=" + rs.getString("soldier_id");

            java.util.Date date = new java.util.Date(System.currentTimeMillis() - Constants.GPS_THRESHOLD_SEC * 1000);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.s");

            formattedDate = sdf.format(date) + "0";

            if (rs.getString("soldier_id") != null) {
                ResultSet rs2 = st2.executeQuery(qry2);


                rs2.next();
                if (formattedDate.compareTo(rs.getString("datetime")) <= 0) {
                    currentLocation.add(1);
                } else {
                    currentLocation.add(0);
                }

                locs.add("'" + rs.getString("soldier_id") + "'");
                locs.add("'" + rs2.getString("username") + "'");
                locs.add(rs.getString("latitude"));
                locs.add(rs.getString("longitude"));
                locs.add("'" + rs.getString("datetime") + "'");
                patrolId.add("'" + rs.getString("patrol_id") + "'");
                patrollingStatus = rs2.getInt("patrolling_status");

            }
        }




    %>



    <div class="container">
        <h2>Soldiers Locations</h2>
        <div class="panel panel-default">
            <div class="panel-heading">Search For Soldiers Locations</div>
            <div class="panel-body">

                <div class="form-group">
                    <div class="col-xs-2">
                        <div class="dropdown">
                            <label>Search Criteria: </label>
                            <button class="btn btn-primary btn-group-vertical dropdown-toggle" type="button" data-toggle="dropdown">Search By
                                <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <%--<li><a tabindex="-1" href="#">Show All</a></li>--%>
                                <%
                                    int adminLevel = Integer.parseInt(session.getAttribute("adminLevel").toString());
                                    ArrayList<String> idLevelUsernameList1 = new ArrayList<>();
                                    ArrayList<String> idLevelUsernameList2 = new ArrayList<>();
                                    ArrayList<String> idLevelUsernameList3 = new ArrayList<>();
                                    ArrayList<String> idLevelUsernameList4 = new ArrayList<>();
                                    ArrayList<String> idLevelUsernameList5 = new ArrayList<>();
                                    ArrayList<String> idLevelUsernameList6 = new ArrayList<>();
                                    String nowUsername = new String();
                                    if (adminLevel <= 6) {
                                        nowUsername = session.getAttribute("username").toString();
                                        if(adminLevel!=6){
                                            idLevelUsernameList1.clear();
                                            idLevelUsernameList1 = RoleService.getIdLevelUsernamesOfAdmin(adminLevel, nowUsername,1);}%>

                                <ul class="dropdown-submenu">
                                    <li>
                                        <form action="currentLocation" method="get">
                                            <input type="hidden" name="viewCriteria" value="<%=adminLevel+"_"+nowUsername%>">
                                            <button class="btn btn-success" type="submit">Show All</button>
                                        </form>
                                    </li>

                                    <%
                                        assert idLevelUsernameList1 != null;
                                        for (int i1 = 0; i1 < idLevelUsernameList1.size(); i1++) {
                                            if (adminLevel <= 5) {
                                                nowUsername = idLevelUsernameList1.get(i1).substring(idLevelUsernameList1.get(i1).split("_")[0].length() + idLevelUsernameList1.get(i1).split("_")[1].length() + 2);
                                                if(adminLevel!=5){idLevelUsernameList2.clear();
                                                    idLevelUsernameList2 = RoleService.getIdLevelUsernamesOfAdmin(adminLevel+1, nowUsername,1);}%>
                                    <a class="test btn btn-danger col col-sm-10" tabindex="-1" href="#"><%=nowUsername%>&nbsp;&nbsp;<span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <form action="currentLocation" method="get">
                                                <input type="hidden" name="viewCriteria" value="<%=(adminLevel+1)+"_"+nowUsername%>">
                                                <button class="btn btn-success" type="submit">Show All</button>
                                            </form>
                                        </li>

                                        <%
                                            assert idLevelUsernameList2 != null;
                                            for (int i2 = 0; i2 < idLevelUsernameList2.size(); i2++) {
                                                if (adminLevel <= 4) {
                                                    nowUsername = idLevelUsernameList2.get(i2).substring(idLevelUsernameList2.get(i2).split("_")[0].length() + idLevelUsernameList2.get(i2).split("_")[1].length() + 2);
                                                    if(adminLevel!=4){idLevelUsernameList3.clear();
                                                        idLevelUsernameList3 = RoleService.getIdLevelUsernamesOfAdmin(adminLevel+2, nowUsername,1);}%>
                                        <a class="test btn btn-danger" tabindex="-1" href="#"><%=nowUsername%>&nbsp;&nbsp;<span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <form action="currentLocation" method="get">
                                                    <input type="hidden" name="viewCriteria" value="<%=(adminLevel+2)+"_"+nowUsername%>">
                                                    <button class="btn btn-success" type="submit">Show All</button>
                                                </form>
                                            </li>
                                            <%
                                                assert idLevelUsernameList3 != null;
                                                for (int i3 = 0; i3 < idLevelUsernameList3.size(); i3++) {
                                                    if (adminLevel <= 3) {
                                                        nowUsername = idLevelUsernameList3.get(i3).substring(idLevelUsernameList3.get(i3).split("_")[0].length() + idLevelUsernameList3.get(i3).split("_")[1].length() + 2);
                                                        if(adminLevel!=3){idLevelUsernameList4.clear();
                                                            idLevelUsernameList4 = RoleService.getIdLevelUsernamesOfAdmin(adminLevel+3, nowUsername,1);}%>
                                            <a class="test btn btn-danger" tabindex="-1" href="#"><%=nowUsername%>&nbsp;&nbsp;<span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <form action="currentLocation" method="get">
                                                        <input type="hidden" name="viewCriteria" value="<%=(adminLevel+3)+"_"+nowUsername%>">
                                                        <button class="btn btn-success" type="submit">Show All</button>
                                                    </form>
                                                </li>
                                                <%
                                                    assert idLevelUsernameList4 != null;
                                                    for (int i4 = 0; i4 < idLevelUsernameList4.size(); i4++) {
                                                        if (adminLevel <= 2) {
                                                            nowUsername = idLevelUsernameList4.get(i4).substring(idLevelUsernameList4.get(i4).split("_")[0].length() + idLevelUsernameList4.get(i4).split("_")[1].length() + 2);
                                                            if(adminLevel!=2){idLevelUsernameList5.clear();
                                                                idLevelUsernameList5 = RoleService.getIdLevelUsernamesOfAdmin(adminLevel+4, nowUsername,1);}%>
                                                <a class="test btn btn-danger" tabindex="-1" href="#"><%=nowUsername%>&nbsp;&nbsp;<span class="caret"></span></a>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <form action="currentLocation" method="get">
                                                            <input type="hidden" name="viewCriteria" value="<%=(adminLevel+4)+"_"+nowUsername%>">
                                                            <button class="btn btn-success" type="submit">Show All</button>
                                                        </form>
                                                    </li>

                                                    <%
                                                        assert idLevelUsernameList5 != null;
                                                        for (int i5 = 0; i5 < idLevelUsernameList5.size(); i5++) {
                                                            if (adminLevel <= 1) {
                                                                nowUsername = idLevelUsernameList5.get(i5).substring(idLevelUsernameList5.get(i5).split("_")[0].length() + idLevelUsernameList5.get(i5).split("_")[1].length() + 2);
                                                                if(adminLevel!=1){
                                                                    idLevelUsernameList6.clear();
                                                                    idLevelUsernameList6 = RoleService.getIdLevelUsernamesOfAdmin(6,nowUsername,1);}%>
                                                    <a class="test btn btn-danger" tabindex="-1" href="#"><%=nowUsername%>&nbsp;&nbsp;<span
                                                            class="caret"></span></a>
                                                    <ul class="dropdown-menu">
                                                        <li>
                                                            <form action="currentLocation" method="get">
                                                                <input type="hidden" name="viewCriteria" value="<%=(adminLevel+5)+"_"+nowUsername%>">
                                                                <button class="btn btn-success" type="submit">Show All</button>
                                                            </form>
                                                        </li>


                                                    </ul>
                                                    <%
                                                            }
                                                        }
                                                    %>


                                                </ul>
                                                <%
                                                        }
                                                    }
                                                %>
                                            </ul>
                                            <%
                                                    }
                                                }
                                            %>
                                        </ul>
                                        <%
                                                }
                                            }
                                        %>
                                    </ul>
                                    <%
                                            }
                                        }
                                    %>
                                </ul>
                                <%}%>
                            </ul>
                            <%--<li>Sylhet</li>
                        <li><a tabindex="-1" href="#">HTML</a></li>
                        <li><a tabindex="-1" href="#">CSS</a></li>
                        <li class="dropdown-submenu">
                            <a class="test" tabindex="-1" href="#">New dropdown <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a tabindex="-1" href="#">2nd level dropdown</a></li>
                                <li><a tabindex="-1" href="#">2nd level dropdown</a></li>
                                <li class="dropdown-submenu">
                                    <a tabindex="-1" href="#">nd level dropdown</a><a class="test" href="#">Another dropdown <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">3rd level dropdown</a></li>
                                        <li><a href="#">3rd level dropdown</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>--%>
                        </div>
                    </div>

                    <form action="currentLocation" class="form-group" method="get">



                        <%--<div class="form-group">
                            <div class="col-xs-3">
                                <label>Soldier: </label>
                                <select class="form-control" id="soldier" name="soldier">
                                    <option value="all">All</option>
                                    <%
                                        Statement st3 = db.connection.createStatement();
                                        String qry3 = "select * from soldier";


                                        ResultSet rs3 = st3.executeQuery(qry3);
                                        while (rs3.next()) {

                                            if(!RoleService.isViewAuthorized(session.getAttribute("viewUsername").toString(),rs3.getString("battalion"))) {
                                                continue;
                                            }
                                    %>
                                    <option value="<%=rs3.getString("id")%>"><%=rs3.getString("id")%>
                                        : <%=rs3.getString("name")%>
                                    </option>
                                    <%
                                        }
                                    %>
                                </select>
                            </div>
                        </div>--%>



                        <div class="col-xs-3 date">
                            <label<%-- class="col-xs-3 control-label"--%>>Date</label>
                            <div class="input-group input-append date" id="datePicker">
                                <input type="text" class="form-control" name="date" placeholder="yyyy-mm-dd"/>
                                <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                        </div>


                        <%--<div class="form-group">
                            <div class="col-xs-3">
                                <label>Date: </label>
                                <input class="form-control" type="date" name="date" placeholder="YYYY-MM-DD">
                            </div>
                        </div>--%>
                        <div class="col-xs-3 dropdown">
                            <label>Options</label>
                            <select name="currentLoc" class="form-control">
                                <option value="true">Current and Last Location</option>
                                <option value="false">All Location and Navigation</option>
                            </select>
                        </div>

                            <div class="col-xs-2">
                                <label>Submit</label>
                                <button class="form-control btn btn-success" type="submit">Search</button>
                            </div>


                    </form>

                </div>




            </div>
        </div>
    </div>





        <div class="row content">



            <%if (request.getParameter("currentLoc") != null && request.getParameter("currentLoc").toString().equals("true")) {%>
            <h3 style="margin-left: 100px; color: darkred"><b>Current and Last Location Search Results</b></h3>
            <%} else {%>
            <h3 style="margin-left: 100px; color: darkblue"><b>Location History</b></h3>
            <%}%>
            <h3 style="margin-left: 100px; color: saddlebrown"><b><%=searchCriteriaStr%></b></h3>
            <div class="row">
                <div><button style="margin-left: 1190px" class="btn btn-primary" value="Reload Page" onClick="history.go(0)">Refresh Map</button></div>
                <div id="map" style="width: 1200px; height: 600px; margin-left: 100px"></div>
            </div>
            <%
                if (session.getAttribute("viewSoldier") != null && !session.getAttribute("viewSoldier").toString().equals("all")) {
            %>
            <script>SelectElement(<%=session.getAttribute("viewSoldier").toString()%>);</script>
            <% }%>


            <%
                int lastPoint = 0;
                java.util.Date date2 = new java.util.Date(System.currentTimeMillis() - Constants.GPS_THRESHOLD_SEC * 1000);
                SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.s");

                String formattedDate2 = sdf2.format(date2) + "0";
                //String bbbbbb=locs.get(4).substring(1,locs.get(4).length()-1);
                if (locs.size() >= 5 && formattedDate2.compareTo(locs.get(4).substring(1, locs.get(4).length() - 1)) <= 0) {
                    lastPoint = 1;
                }

            %>

            <script type="text/javascript">
                var locations = <%=locs%>;
                var threeColor = <%=threeColor%>;
                var lastPoint = <%=lastPoint%>;
                var currentLocation = <%=currentLocation%>;
                var patrolId = <%=patrolId%>;
                var patrollingStatus = <%=patrollingStatus%>;

                var centerPointX = <%=Float.parseFloat(session.getAttribute("centerPointX").toString())%>;
                var centerPointY = <%=Float.parseFloat(session.getAttribute("centerPointY").toString())%>;
                var zoom = <%=session.getAttribute("autoZoom").toString()%>;

                if(threeColor == 1 && locations.length>=5) {
                    centerPointX = locations[2];
                    centerPointY = locations[3];
                    zoom = 16;
                }

                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: zoom,
                    center: new google.maps.LatLng(centerPointX, centerPointY),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });

                var infowindow = new google.maps.InfoWindow();

                var marker, i;

                var mk = ['blue-dot', 'red-dot', 'yellow-dot', 'green-dot'];


                for (i = 0 + (threeColor * 5); i < locations.length; i += 5) {
                    (function(){
                    var status="";
                    var colorIndex = locations[i] % 4;
                    var startPatrol = 0;
                    var endPatrol = 0;
                    if (threeColor == 1 && i == 0) {
                        colorIndex = 1;
                    }
                    else if (threeColor == 1 && i > 0 && !(patrolId[i / 5] === patrolId[i / 5 - 1])) {
                        colorIndex = 1;
                        endPatrol = 1;
                    }
                    else if (threeColor == 1 && (i + 5) == locations.length) {
                        colorIndex = 3;
                    }
                    else if (threeColor == 1 && (i + 5) < locations.length && !(patrolId[i / 5] === patrolId[i / 5 + 1])) {
                        startPatrol = 1;
                        colorIndex = 1;
                    }
                    else if (threeColor == 1) {
                        colorIndex = 0;
                    }


                    if ((i / 5) < currentLocation.length) {
                        if (currentLocation[i / 5] == 1) {
                            colorIndex = 3;
                        }
                        else {
                            colorIndex = 1;
                        }
                    }


                    if(threeColor==1 && (i+5)==locations.length){
                        var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
                        status = "patrolling started here";
                        marker = new google.maps.Marker({

                            icon: image,
                            position: new google.maps.LatLng(locations[i + 2], locations[i + 3]),
                            animation: google.maps.Animation.DROP,
                            map: map,
                            label: "start"


                        });
                    }
                    else if (startPatrol == 1) {
                        var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
                        status= "patrolling started here";
                        marker = new google.maps.Marker({

                            icon: image,
                            position: new google.maps.LatLng(locations[i + 2], locations[i + 3]),
                            animation: google.maps.Animation.DROP,
                            map: map,
                            label: "start"
                        });
                    }
                    else if (endPatrol == 1) {
                        var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';

                        status= "patrolling ended here";
                        marker = new google.maps.Marker({

                            icon: image,
                            position: new google.maps.LatLng(locations[i + 2], locations[i + 3]),
                            animation: google.maps.Animation.DROP,
                            map: map,
                            label: "end"
                        });
                    }
                    else {
                        status="";
                        marker = new google.maps.Marker({

                            icon: 'http://maps.google.com/mapfiles/ms/icons/' + mk[colorIndex] + '.png',
                            position: new google.maps.LatLng(locations[i + 2], locations[i + 3]),
                            animation: google.maps.Animation.DROP,
                            map: map
                        });
                    }
                    google.maps.event.addListener(marker, 'click', (function (marker, i) {
                        return function () {
                            if ((i / 5) < currentLocation.length && currentLocation[i / 5] == 1) {
                                infowindow.setContent("id: " + locations[i] + "<br>username: " + locations[i + 1] + "<br>time: " + locations[i + 4] + "<br>patrol id: " + patrolId[i / 5] + "<br>Current Status: Active" + "<br><b>" + status+"</b>");
                            }
                            else if ((i / 5) < currentLocation.length && currentLocation[i / 5] == 0) {
                                infowindow.setContent("id: " + locations[i] + "<br>username: " + locations[i + 1] + "<br>time: " + locations[i + 4] + "<br>patrol id: " + patrolId[i / 5] + "<br>Current Status: Inactive" + "<br><b>" + status+"</b>");
                            }
                            else {
                                infowindow.setContent("id: " + locations[i] + "<br>username: " + locations[i + 1] + "<br>time: " + locations[i + 4] + "<br>patrol id: " + patrolId[i / 5] + "<br><b>" + status+"</b>");
                            }
                            infowindow.open(map, marker);

                        }
                    })(marker, i));

                    if (threeColor == 1 && patrolId[i / 5] === patrolId[i / 5 - 1]) {
                        var line = new google.maps.Polyline({
                                path: [
                                    new google.maps.LatLng(locations[i + 2], locations[i + 3]),
                                    new google.maps.LatLng(locations[i + 2 - 5], locations[i + 3 - 5])
                                ],
                                strokeColor: "#FF0000",
                                strokeOpacity: 1.0,
                                strokeWeight: 2,
                                map: map
                            }
                        );

                    }

                    })();
                }


                if (threeColor == 1) {


                    /*i=locations.length-5;
                     colorIndex=3;
                     var image= 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';

                     marker = new google.maps.Marker({

                     icon: image,
                     position: new google.maps.LatLng(locations[i+2], locations[i+3]),
                     map: map,
                     label: "start"

                     });
                     google.maps.event.addListener(marker, 'click', (function(marker, i) {
                     return function() {

                     infowindow.setContent("id: "+locations[i]+"<br>name: "+locations[i+1]+"<br>time: "+locations[i+4]+"<br>patrol id: "+patrolId[i/5]);
                     infowindow.open(map, marker);

                     }
                     })(marker, i));*/




                    /*//for start patrolling

                    image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
                    label = "start";
                    status = "patrolling started here";
                    i = locations.length-5;
                    colorIndex = 1;
                    marker = new google.maps.Marker({

                        icon: image,
                        position: new google.maps.LatLng(locations[i + 2], locations[i + 3]),
                        animation: google.maps.Animation.DROP,
                        map: map,
                        label: label
                    });

                    google.maps.event.addListener(marker, 'click', (function (marker, i) {
                        return function () {

                            infowindow.setContent("id: " + locations[i] + "<br>name: " + locations[i + 1] + "<br>time: " + locations[i + 4] + "<br>patrol id: " + patrolId[i / 5] + "<br>status: " + status);
                            infowindow.open(map, marker);

                        }
                    })(marker, i));*/






                    (function(){
                    var label;
                    status = "";
                    if (lastPoint == 1) {
                        image = 'http://maps.google.com/mapfiles/ms/icons/' + mk[3] + '.png';
                        label = "patrolling";
                        status = "currently patrolling here";
                    }
                    else if (patrollingStatus == 1) {
                        image = 'http://maps.google.com/mapfiles/ms/icons/' + mk[2] + '.png';
                        label = "paused";
                        status = "patrolling paused or network insufficiency";
                    }
                    else {
                        image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
                        label = "end";
                        status = "patrolling ended here";
                    }
                    colorIndex = 1;
                    i=0;

                    marker = new google.maps.Marker({

                        icon: image,
                        position: new google.maps.LatLng(locations[i + 2], locations[i + 3]),
                        animation: google.maps.Animation.DROP,
                        map: map,
                        label: label
                    });

                    google.maps.event.addListener(marker, 'click', (function (marker, i) {
                        return function () {

                            infowindow.setContent("id: " + locations[i] + "<br>username: " + locations[i + 1] + "<br>time: " + locations[i + 4] + "<br>patrol id: " + patrolId[i / 5] + "<br><b>" + status+"</b>");
                            infowindow.open(map, marker);

                        }
                    })(marker, i));
                    })();
                }


                //BOP Positioning

                <%while (rsPillar.next()){

                if(!RoleService.isViewAuthorized(session.getAttribute("viewUsername").toString(),rsPillar.getString("battalion"))) {
                continue;
            }
                %>
                /*image = 'http://maps.google.com/mapfiles/ms/icons/' + mk[0] + '.png';*/
                image = 'resources/images/border-s.png';
                marker = new google.maps.Marker({

                    icon: image,
                    position: new google.maps.LatLng(<%=rsPillar.getString("latitude")%>, <%=rsPillar.getString("longitude")%>),
                    animation: google.maps.Animation.DROP,
                    map: map,

                });
                /*marker.setAnimation(google.maps.Animation.BOUNCE);*/
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {

                        infowindow.setContent("<b>Pillar<b><br>id: "+<%="'"+rsPillar.getString("id")+"'"%>+
                        "<br>name: "+<%="'"+rsPillar.getString("name")+"'"%>+
                        "<br>number: "+<%="'"+rsPillar.getString("number")+"'"%>+
                        "<br>status: " +
                        <%="'"+rsPillar.getString("situation")+"'"%>)
                        ;
                        infowindow.open(map, marker);

                    }
                })(marker, i));
                <%}%>

            </script>


        </div>
    </div>
</div>
<br><br>
<%
    }catch (Exception e){
        response.sendRedirect("somethingWentWrong");
    }finally {
        db.close();
    }


%>
<%@ include file="footer.jsp" %>


