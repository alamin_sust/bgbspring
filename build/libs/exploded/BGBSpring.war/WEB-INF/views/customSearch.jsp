<%-- 
    Document   : customSearch
    Created on : Feb 11, 2017, 1:17:30 AM
    Author     : Al-Amin
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="net.bgb.monitoring.connection.Database"%>
<%@ page import="net.bgb.monitoring.web.util.Constants" %>
<%@ page import="net.bgb.monitoring.service.RoleService" %>


<%@ include file="header.jsp" %>



<%

                Database db = new Database();
                db.connect();
                try{
                Statement st = db.connection.createStatement();
                String qry = "select * from soldier where id>="+ Constants.SOLDIER_STARTING_INDEX;
                ResultSet rs = st.executeQuery(qry);
  
                Statement st2 = db.connection.createStatement();
                String qry2 = "select * from pillar where id>="+ Constants.PILLAR_STARTING_INDEX;
                ResultSet rs2 = st2.executeQuery(qry2);

%>    
    

  <%
      if(request.getParameter("name")!=null){
          %>
<div class="container-fluid text-center">  
     <div class="alert alert-success fade in">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">X</a>
        <p><strong>Success!</strong></p>
        Pillar Inserted Into Central Database Successfully.
      </div>
    <%
        request.setAttribute("name", null);
        
    }%>
  <div class="row content">
    <div class="col-sm-2 sidenav">
    </div>
    <div class="col-sm-8 text-left">
        <legend style="font-size: 35px">Search by Different Criteria</legend>
      <br>
      <form class="form-inline" method="get" action="customSearchResult">
      
      <b>From :</b>
      <br>
      Date: <input type="date" name="fromDate" class="form-control">&nbsp;Time: <input type="time" name="fromTime" class="form-control">
      <br><br>
      <b>To :</b>
      <br>
      Date: <input type="date" name="toDate" class="form-control">&nbsp;Time: <input type="time" name="toTime" class="form-control">
      <br><br>
      <b>Soldier :</b>
          <div class="form-group">
              <div class="col-xs-3">
      <select class="form-control" name="soldierId">
          <option value="">select</option>
          <%
          while(rs.next()){
              if(!RoleService.isViewAuthorized(session.getAttribute("username").toString(),rs.getString("battalion"))){
                 continue;
              }
          %>
          <option value=<%=rs.getString("id")%>><%=rs.getString("name")+" (username: "+rs.getString("battalion")+")"%></option>
        <%}%>
      </select>
              </div></div>
      <br><br>
      <b>Pillar :</b>
          <div class="form-group">
              <div class="col-xs-3">
      <select class="form-control" name="pillarId">
          <option value="">select</option>
        <%
          while(rs2.next()){
              if(!RoleService.isViewAuthorized(session.getAttribute("username").toString(),rs2.getString("battalion"))){
                  continue;
              }
          %>
          <option value=<%=rs2.getString("id")%>><%=rs2.getString("name")/*+" (pillar number: "+rs2.getString("number")+")"*/%></option>
        <%}%>
      </select></div></div>
      <br><br>
      <button type="submit" class="btn btn-danger">Search</button>
      </form>
    </div>
    <div class="col-sm-2 sidenav">
      
    </div>
  </div>
</div>
<br><br><br><br><br><br><br><br><br>
<%
    }catch (Exception e){
        response.sendRedirect("somethingWentWrong");
    }finally {
        db.close();
    }


%>
<%@ include file="footer.jsp" %>
