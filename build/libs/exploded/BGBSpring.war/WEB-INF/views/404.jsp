<%--
  Created by IntelliJ IDEA.
  User: Al-Amin
  Date: 3/7/2017
  Time: 8:47 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <%--<meta http-equiv="refresh" content="5" />--%>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>404 Not Found.</title>
    <style>
        .center {text-align: center; margin-left: auto; margin-right: auto; margin-bottom: auto; margin-top: auto;}
    </style>
</head>
<body>
<div class="container">
    <br><br><br><br><br><br>
    <div class="row">
        <div class="span12">
            <div class="hero-unit center">
                <h1>Page Not Found <small><font face="Tahoma" color="red">Error 404</font></small></h1>
                <br />
                <p>The page you requested could not be found, either contact your webmaster or try again. Use your browsers <b>Back</b> button to navigate to the page you have prevously come from</p>
                <p><b>Or you could just press this neat little button:</b></p>
                <a href="home" class="btn btn-large btn-info"><i class="icon-home icon-white"></i> Take Me Home</a>
                <br><br>
                <p style="color: #555555; margin-top: auto"><span class="glyphicon glyphicon-copyright-mark"></span><b> Border Guard of Bangladesh(BGB)</b></p>
                <p style="color: darkblue"><b>Web Developed by Md. Al- Amin</b></p>
            </div>
        </div>
    </div>
</div>


</body>
</html>
