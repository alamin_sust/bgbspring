<%--
    Document   : pillarHistory
    Created on : Feb 26, 2017, 2:02:16 AM
    Author     : Al-Amin
--%>


<%@page import="net.bgb.monitoring.connection.Database" %>
<%@page import="java.io.File" %>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.Collections" %>
<%@page import="java.util.List" %>
<%@ page import="net.bgb.monitoring.web.util.StringConversion" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="net.bgb.monitoring.service.RoleService" %>
<%@ include file="header.jsp" %>

<h3 style="margin-left: 100px">History of <%=request.getParameter("name")%></h3 style="margin-left: 100px">
<%
    Database db = new Database();
    db.connect();
    try{

        List<String> imageUrlList = new ArrayList<>();
        int iter = 0;
        File imageDir = new File("C:\\Users\\Al-Amin\\Documents\\NetBeansProjects\\BGBSpring\\src\\main\\webapp\\resources\\images\\pillarImg");
        for (File imageFile : imageDir.listFiles()) {
            String imageFileName = imageFile.getName();

            // add this images name to the list we are building up
            if (request.getParameter("id") != null && request.getParameter("id").equals(imageFileName.split("_")[0])) {
                imageUrlList.add(imageFileName);
            }

        }

        if (imageUrlList.size() > 0) {
            Collections.sort(imageUrlList.subList(1, imageUrlList.size()));
        }
        for (iter = 0; iter < imageUrlList.size(); iter++) {
            String imgUrl = "/resources/images/pillarImg/" + imageUrlList.get(iter);


            Statement st  = db.connection.createStatement();
            String qry = "select * from pillar_updated_by where img_url like '"+imageUrlList.get(iter)+"'";
            ResultSet rs = st.executeQuery(qry);




            String capturedBy = Constants.NOT_AVAILABLE;

            if(rs.next()){


                Statement st2  = db.connection.createStatement();
                String qry2 = "select * from soldier where id="+rs.getString("soldier_id");
                ResultSet rs2 = st2.executeQuery(qry2);
                if(rs2.next()){
                    capturedBy=rs2.getString("name")+" (id: "+rs2.getString("id")+", username: "+rs2.getString("username")+")";
                }}



%>

<%
    if ((iter % 3) == 0) {
%>
<div class="container">
    <div class="row">
        <%}%>
        <div class="col-sm-4">
            <div class="panel panel-primary">
                <div class="panel-heading">Date Captured: <%=StringConversion.nullReplace(imageUrlList.get(iter).split("_")[3]+
                        "/"+imageUrlList.get(iter).split("_")[2]+"/"+imageUrlList.get(iter).split("_")[1],Constants.NOT_AVAILABLE)%>
                </div>
                <div class="panel-body"><img src=
                                             <c:url value="<%=imgUrl%>"/> class="img-responsive"
                                             style="width:100%; height: 300px" alt="Image"></div>
                <div class="panel-footer">Captured By: <%=capturedBy%>
                </div>
                <div class="panel-footer">Time Captured: <%=StringConversion.nullReplace(imageUrlList.get(iter).split("_")[4]+
                        ":"+imageUrlList.get(iter).split("_")[5]+":"+imageUrlList.get(iter).split("_")[6],Constants.NOT_AVAILABLE)%>
                </div>
                <div class="panel-footer">Status: <%=StringConversion.nullReplace(imageUrlList.get(iter).split("_")[7],Constants.NOT_AVAILABLE)%>
                </div>
            </div>
        </div>

        <%
            if (((iter + 1) % 3) == 0) {
        %>
    </div>
</div>
<br>
<%}%>

<%
    }
    if ((iter % 3) != 0) {
%>
</div>
</div><br>
<%}if(iter==0){%>
<legend style="margin-left: 300px;">Nothing Found</legend><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<%}
}catch (Exception e){
    response.sendRedirect("somethingWentWrong");
}finally {
    db.close();
}


%>


<%@ include file="footer.jsp" %>