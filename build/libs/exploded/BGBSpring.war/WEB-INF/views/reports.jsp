<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="net.bgb.monitoring.connection.Database" %>
<%@ page import="net.bgb.monitoring.web.util.StringConversion" %><%--
  Created by IntelliJ IDEA.
  User: Al-Amin
  Date: 3/17/2017
  Time: 8:09 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="header.jsp" %>
<%

    Database db = new Database();
    db.connect();
    try{

%>
<div class="container text-center">

    <div class="row">

        <div class="col-sm-12 well">
            <div class="well">
                <h4><b>Search Criteria</b></h4>
            </div>
            <div>
                <form action="reports" method="get">
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-3">
                            <label>Date </label>
                            <input class="form-control" type="date" name="date" placeholder="Date">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2">
                            <label>Submit</label>
                            <button class="form-control btn btn-success" type="submit">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
            <br><br><br><br><br><br>

            <div class="well" style="color: red">
                <h3><b>Reports</b></h3>
            </div>

            <%

                Statement st4 = db.connection.createStatement();
                String qry4 = "select * from report_to_bgb";
                if(request.getParameter("date")!=null && !request.getParameter("date").equals("null")) {
                    qry4+=" where datetime like '"+request.getParameter("date")+"%'";
                }
                qry4+=" order by datetime desc";
                ResultSet rsReport  = st4.executeQuery(qry4);
                while (rsReport.next()){
            %>

            <div class="col-sm-18">
                <div class="well">
                    <p><strong>Time of Report:</strong> <b style="color: red"><%=StringConversion.nullReplace(rsReport.getString("datetime"),Constants.NOT_AVAILABLE)%></b></p>
                    <p><strong>Name: </strong><b style="color: green"><%=StringConversion.nullReplace(rsReport.getString("name"),Constants.NOT_PROVIDED)%></b> </p>
                    <p><strong>Mobile No.:</strong><b style="color: green"> <%=StringConversion.nullReplace(rsReport.getString("mobile"),Constants.NOT_PROVIDED)%></b> </p>
                    <p><strong>Address:</strong><b style="color: green"> <%=StringConversion.nullReplace(rsReport.getString("address"),Constants.NOT_PROVIDED)%></b> </p>
                    <p><strong>Message:</strong></p>
                    <p style="color: green"><strong> <%=StringConversion.nullReplace(rsReport.getString("message"),Constants.NOT_PROVIDED)%></strong> </p>
                </div>
            </div>
            <%
                }
            %>
        </div>
    </div>
</div>
<%
    }catch (Exception e){
        response.sendRedirect("somethingWentWrong");
    }finally {
        db.close();
    }


%>
<%@ include file="footer.jsp" %>
