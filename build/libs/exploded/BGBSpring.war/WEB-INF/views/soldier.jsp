<%--
    Document   : soldier
    Created on : Feb 9, 2017, 1:02:14 AM
    Author     : Al-Amin
--%>
<%@page import="net.bgb.monitoring.connection.Database" %>
<%@page import="net.bgb.monitoring.service.RoleService" %>
<%@page import="net.bgb.monitoring.web.util.StringConversion" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.util.ArrayList" %>
<%@ include file="header.jsp" %>
<%

    Database db = new Database();
    db.connect();
    try {

%>
<div class="container">
    <h2>Soldiers</h2>
    <div class="panel panel-default">
            <div class="panel-heading">Search For Soldiers</div>
            <div class="panel-body">

                <div class="form-group">
                    <div class="col-xs-2">
                        <div class="dropdown">
                            <label>Search Criteria: </label>
                            <button class="btn btn-primary btn-group-vertical dropdown-toggle" type="button" data-toggle="dropdown">Search By
                                <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <%--<li><a tabindex="-1" href="#">Show All</a></li>--%>
                                <%
                                    int adminLevel = Integer.parseInt(session.getAttribute("adminLevel").toString());
                                    ArrayList<String> idLevelUsernameList1 = new ArrayList<>();
                                    ArrayList<String> idLevelUsernameList2 = new ArrayList<>();
                                    ArrayList<String> idLevelUsernameList3 = new ArrayList<>();
                                    ArrayList<String> idLevelUsernameList4 = new ArrayList<>();
                                    ArrayList<String> idLevelUsernameList5 = new ArrayList<>();
                                    ArrayList<String> idLevelUsernameList6 = new ArrayList<>();
                                    String nowUsername = new String();
                                    if (adminLevel <= 6) {
                                        nowUsername = session.getAttribute("username").toString();
                                        if(adminLevel!=6){
                                            idLevelUsernameList1.clear();
                                            idLevelUsernameList1 = RoleService.getIdLevelUsernamesOfAdmin(adminLevel, nowUsername,1);}%>

                                <ul class="dropdown-submenu">
                                    <li>
                                        <form action="soldier" method="get">
                                            <input type="hidden" name="viewCriteria" value="<%=adminLevel+"_"+nowUsername%>">
                                            <button class="btn btn-success" type="submit">Show All</button>
                                        </form>
                                    </li>

                                    <%
                                        assert idLevelUsernameList1 != null;
                                        for (int i1 = 0; i1 < idLevelUsernameList1.size(); i1++) {
                                            if (adminLevel <= 5) {
                                                    nowUsername = idLevelUsernameList1.get(i1).substring(idLevelUsernameList1.get(i1).split("_")[0].length() + idLevelUsernameList1.get(i1).split("_")[1].length() + 2);
                                                    if(adminLevel!=5){idLevelUsernameList2.clear();
                                                        idLevelUsernameList2 = RoleService.getIdLevelUsernamesOfAdmin(adminLevel+1, nowUsername,1);}%>
                                    <a class="test btn btn-danger col col-sm-10" tabindex="-1" href="#"><%=nowUsername%>&nbsp;&nbsp;<span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <form action="soldier" method="get">
                                            <input type="hidden" name="viewCriteria" value="<%=(adminLevel+1)+"_"+nowUsername%>">
                                            <button class="btn btn-success" type="submit">Show All</button>
                                        </form>
                                        </li>

                                        <%
                                            assert idLevelUsernameList2 != null;
                                            for (int i2 = 0; i2 < idLevelUsernameList2.size(); i2++) {
                                                if (adminLevel <= 4) {
                                                        nowUsername = idLevelUsernameList2.get(i2).substring(idLevelUsernameList2.get(i2).split("_")[0].length() + idLevelUsernameList2.get(i2).split("_")[1].length() + 2);
                                                        if(adminLevel!=4){idLevelUsernameList3.clear();
                                                            idLevelUsernameList3 = RoleService.getIdLevelUsernamesOfAdmin(adminLevel+2, nowUsername,1);}%>
                                        <a class="test btn btn-danger" tabindex="-1" href="#"><%=nowUsername%>&nbsp;&nbsp;<span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <form action="soldier" method="get">
                                                    <input type="hidden" name="viewCriteria" value="<%=(adminLevel+2)+"_"+nowUsername%>">
                                                    <button class="btn btn-success" type="submit">Show All</button>
                                                </form>
                                            </li>
                                            <%
                                                assert idLevelUsernameList3 != null;
                                                for (int i3 = 0; i3 < idLevelUsernameList3.size(); i3++) {
                                                    if (adminLevel <= 3) {
                                                            nowUsername = idLevelUsernameList3.get(i3).substring(idLevelUsernameList3.get(i3).split("_")[0].length() + idLevelUsernameList3.get(i3).split("_")[1].length() + 2);
                                                            if(adminLevel!=3){idLevelUsernameList4.clear();
                                                                idLevelUsernameList4 = RoleService.getIdLevelUsernamesOfAdmin(adminLevel+3, nowUsername,1);}%>
                                            <a class="test btn btn-danger" tabindex="-1" href="#"><%=nowUsername%>&nbsp;&nbsp;<span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <form action="soldier" method="get">
                                                        <input type="hidden" name="viewCriteria" value="<%=(adminLevel+3)+"_"+nowUsername%>">
                                                        <button class="btn btn-success" type="submit">Show All</button>
                                                    </form>
                                                </li>
                                                <%
                                                    assert idLevelUsernameList4 != null;
                                                    for (int i4 = 0; i4 < idLevelUsernameList4.size(); i4++) {
                                                        if (adminLevel <= 2) {
                                                                nowUsername = idLevelUsernameList4.get(i4).substring(idLevelUsernameList4.get(i4).split("_")[0].length() + idLevelUsernameList4.get(i4).split("_")[1].length() + 2);
                                                                if(adminLevel!=2){idLevelUsernameList5.clear();
                                                                    idLevelUsernameList5 = RoleService.getIdLevelUsernamesOfAdmin(adminLevel+4, nowUsername,1);}%>
                                                <a class="test btn btn-danger" tabindex="-1" href="#"><%=nowUsername%>&nbsp;&nbsp;<span class="caret"></span></a>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <form action="soldier" method="get">
                                                            <input type="hidden" name="viewCriteria" value="<%=(adminLevel+4)+"_"+nowUsername.replace("/","+")%>">
                                                            <button class="btn btn-success" type="submit">Show All</button>
                                                        </form>
                                                    </li>

                                                    <%
                                                        assert idLevelUsernameList5 != null;
                                                        for (int i5 = 0; i5 < idLevelUsernameList5.size(); i5++) {
                                                            if (adminLevel <= 1) {
                                                                    nowUsername = idLevelUsernameList5.get(i5).substring(idLevelUsernameList5.get(i5).split("_")[0].length() + idLevelUsernameList5.get(i5).split("_")[1].length() + 2);
                                                        if(adminLevel!=1){
                                                                        idLevelUsernameList6.clear();
                                                            idLevelUsernameList6 = RoleService.getIdLevelUsernamesOfAdmin(6,nowUsername,1);}%>
                                                    <a class="test btn btn-danger" tabindex="-1" href="#"><%=nowUsername%>&nbsp;&nbsp;<span
                                                                class="caret"></span></a>
                                                    <ul class="dropdown-menu">
                                                        <li>
                                                            <form action="soldier" method="get">
                                                                <input type="hidden" name="viewCriteria" value="<%=(adminLevel+5)+"_"+nowUsername%>">
                                                                <button class="btn btn-success" type="submit">Show All</button>
                                                            </form>
                                                        </li>


                                                    </ul>
                                                    <%
                                                                    }
                                                            }
                                                    %>


                                                </ul>
                                                <%
                                                                }
                                                        }
                                                %>
                                            </ul>
                                            <%
                                                            }
                                                    }
                                            %>
                                        </ul>
                                        <%
                                                        }
                                                }
                                        %>
                                    </ul>
                                    <%
                                                    }
                                            }
                                    %>
                                </ul>
                                <%}%>
                            </ul>
                            <%--<li>Sylhet</li>
                        <li><a tabindex="-1" href="#">HTML</a></li>
                        <li><a tabindex="-1" href="#">CSS</a></li>
                        <li class="dropdown-submenu">
                            <a class="test" tabindex="-1" href="#">New dropdown <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a tabindex="-1" href="#">2nd level dropdown</a></li>
                                <li><a tabindex="-1" href="#">2nd level dropdown</a></li>
                                <li class="dropdown-submenu">
                                    <a tabindex="-1" href="#">nd level dropdown</a><a class="test" href="#">Another dropdown <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">3rd level dropdown</a></li>
                                        <li><a href="#">3rd level dropdown</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>--%>
                        </div>
                    </div>
                </div>

            </div>
        </div>
</div>


<%
    int iter = 0;
    String query = "select * from soldier where id>=" + Constants.SOLDIER_STARTING_INDEX;

    if(request.getParameter("viewCriteria")!=null && !request.getParameter("viewCriteria").isEmpty()) {

        int viewLevel = Integer.parseInt(request.getParameter("viewCriteria").split("_")[0]);


        if(viewLevel == 6) {
            query+= " and username like '"+request.getParameter("viewCriteria").substring(request.getParameter("viewCriteria").split("_")[0].length()+1).replace("+","/")+"'";
        } else {
            session.setAttribute("viewUsername",request.getParameter("viewCriteria").substring(request.getParameter("viewCriteria").split("_")[0].length()+1));
        }



    }









            if (request.getParameter("findsoldier") != null) {
                    String search = request.getParameter("findsoldier").toString();
                    query += " and (name like '" + search + "' or designation like '" + search + "')";
        }

                    Statement st = db.connection.createStatement();

            query+=" limit 50";

            ResultSet rs = st.executeQuery(query);

                    for (iter = 0; rs.next(); ) {
                    if (!RoleService.isViewAuthorized(session.getAttribute("viewUsername").toString(), rs.getString("battalion"))) {
                            continue;
                        }
%>

<%
        if ((iter % 3) == 0) {
%>
    <div class="container">
        <div class="row">
            <%}%>
            <div class="col-sm-4">
                <div class="panel panel-primary">
                    <div class="panel-heading"><%=StringConversion.nullReplace(rs.getString("name"), Constants.NOT_AVAILABLE)%>
                    </div>
                    <div class="panel-body"><img src=
                                                                                                       <c:url value="/resources/images/soldier.jpg"/> class="img-responsive"
                                                                                               style="width:100%" alt="Image"></div>
                    <div class="panel-footer">
                        <strong>Designation: <%=StringConversion.nullReplace(rs.getString("designation"), Constants.NOT_AVAILABLE)%>
                        </strong></div>
                </div>
            </div>

            <%
                if (((iter + 1) % 3) == 0) {
            %>
</div>
    </div>
<br>
<%}%>

<%
        iter++;
        }
        if ((iter % 3) != 0) {
%>
</div>
</div><br>
    <%
            }
        if (iter == 0) {
    %>
<legend style="margin-left: 300px;">Nothing Found</legend>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<%
                    }
            } catch (Exception e) {
    response.sendRedirect("somethingWentWrong");
        } finally {
    db.close();
}



%>


<%@ include file="footer.jsp" %>