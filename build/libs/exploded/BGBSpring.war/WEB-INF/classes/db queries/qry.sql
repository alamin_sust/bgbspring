update pillar set battalion=28 WHERE id>=99 and id<=384;
update pillar set battalion=41 WHERE id>=385 and id<=556;
update pillar set battalion=48 WHERE id>=557 and id<=714;

update pillar_updated_by set battalion=28 WHERE soldier_id=32 or soldier_id=40;
update pillar_updated_by set battalion=41 WHERE soldier_id=37 or soldier_id=38 or soldier_id=39;
update pillar_updated_by set battalion=48 WHERE soldier_id=33 or soldier_id=34 or soldier_id=35 or soldier_id=36;

update soldier set battalion=28 WHERE id=32 or id=40;
update soldier set battalion=41 WHERE id=37 or id=38 or id=39;
update soldier set battalion=48 WHERE id=33 or id=34 or id=35 or id=36;

/*07-04-2017 2:41 am server */

update soldier set battalion=28 WHERE id=32 or id=40 or id=46 or id=47 or id=48;
update soldier set battalion=41 WHERE id=34 or id=38 or id=39 or id=42 or id=43 or id=50;
update soldier set battalion=48 WHERE id=33 or id=35 or id=36 or id=37 or id=44 or id=45 or id=49;

update pillar_updated_by set battalion=28 WHERE soldier_id=32 or soldier_id=40 or soldier_id=46 or soldier_id=47 or soldier_id=48;
update pillar_updated_by set battalion=41 WHERE soldier_id=34 or soldier_id=38 or soldier_id=39 or soldier_id=42 or soldier_id=43 or soldier_id=50;
update pillar_updated_by set battalion=48 WHERE soldier_id=33 or soldier_id=35 or soldier_id=36 or soldier_id=37 or soldier_id=44 or soldier_id=45 or soldier_id=49;

update soldier_location set battalion=28 WHERE soldier_id=32 or soldier_id=40 or soldier_id=46 or soldier_id=47 or soldier_id=48;
update soldier_location set battalion=41 WHERE soldier_id=34 or soldier_id=38 or soldier_id=39 or soldier_id=42 or soldier_id=43 or soldier_id=50;
update soldier_location set battalion=48 WHERE soldier_id=33 or soldier_id=35 or soldier_id=36 or soldier_id=37 or soldier_id=44 or soldier_id=45 or soldier_id=49;